#ifndef _MENU_H_
#define _MENU_H_

typedef enum
{
	MENU_NORMAL,
	MENU_BACK,
	MENU_RESUME,
	MENU_OKCANCEL,
	MENU_OKCANCELAPPLY
} menu_type_enum;

typedef enum
{
	MENU_ITEM_DESTROY,
	MENU_ITEM_SUBMENU,
	MENU_ITEM_ONOFF,
	MENU_ITEM_INTEGER,
	MENU_ITEM_STRINGS,
	MENU_ITEM_STRING
} menu_item_type_enum;

typedef struct _menu_struct menu_struct;

typedef struct
{
	unsigned char id;
	menu_item_type_enum type;
	char *title;

	union
	{
		int return_val; //MENU_ITEM_DESTROY
		menu_struct *submenu; //MENU_ITEM_SUBMENU
		unsigned char on; //MENU_ITEM_ONOFF
		
		struct
		{
			int lower_boundary, upper_boundary, val;
		} integer; //MENU_ITEM_INTEGER

		struct
		{
			unsigned char count, val;
			char **string;
		} strings; //MENU_ITEM_STRINGS

		struct
		{
			unsigned short len, max_len;
			char *string;
		} string; //MENU_ITEM_STRING
	} data;
} menu_item_struct;

struct _menu_struct
{
	//Menu properties.
	menu_type_enum type;
	const char *title;
	menu_struct *parent;
	
	//Menu items.
	unsigned char item_count;
	menu_item_struct *item;

	//Call-backs.
	void ( *apply )( void *userdata, menu_struct *menu );
	void *apply_userdata;
	void ( *terminate )( void *userdata, menu_struct *menu );
	void *terminate_userdata;
};

menu_struct *menu_create( menu_struct *parent, menu_type_enum type, const char *title );
menu_item_struct *menu_append_destroy_item( menu_struct *menu, unsigned char id, char *title, int return_val );
menu_item_struct *menu_append_submenu_item( menu_struct *menu, unsigned char id, char *title, menu_struct *submenu );
menu_item_struct *menu_append_onoff_item( menu_struct *menu, unsigned char id, char *title, unsigned char on );
menu_item_struct *menu_append_integer_item( menu_struct *menu, unsigned char id, char *title, int lower_boundary, int upper_boundary, int val );
menu_item_struct *menu_append_strings_item( menu_struct *menu, unsigned char id, char *title, unsigned char index, unsigned char string_count, ... );
menu_item_struct *menu_append_string_item( menu_struct *menu, unsigned char id, char *title, unsigned short len, unsigned short max_len, char *string );
void menu_free( menu_struct *menu );

#endif
