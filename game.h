#ifndef _GAME_H_
#define _GAME_H_

#include "video.h"
#include "input.h"

#define GAME_WIDTH 192
#define GAME_HEIGHT 352
#define BOARD_WIDTH 160
#define BOARD_HEIGHT 320
#define BORDER_WIDTH 16
#define BORDER_HEIGHT 16
#define ROTATION_COUNT 4

enum
{
	TEXTURE_BLOCK,
	TEXTURE_BACKGROUND,
	TEXTURE_HBORDER,
	TEXTURE_LBORDER,
	TEXTURE_RBORDER,
	TEXTURE_COUNT
};

enum
{
	GAME_KEY_LEFT,
	GAME_KEY_RIGHT,
	GAME_KEY_ROTATE_CLOCKWISE,
	GAME_KEY_ROTATE_COUNTERCLOCKWISE,
	GAME_KEY_TURBO,
	GAME_KEY_DROP,
	GAME_KEY_COUNT
};

enum
{
	SYSTEM_KEY_PAUSE,
	SYSTEM_KEY_QUIT,
	SYSTEM_KEY_FULLSCREEN,
	SYSTEM_KEY_COUNT
};

enum
{
	SHAPE_T,
	SHAPE_I,
	SHAPE_S,
	SHAPE_Z,
	SHAPE_L,
	SHAPE_J,
	SHAPE_D,
	SHAPE_COUNT
};

typedef struct
{
	unsigned char width, height;
	char offsetx, offsety;
	unsigned char *array;
} shape_rotation_struct;

typedef struct
{
	float r, g, b;
} colour_struct;

typedef struct
{
	shape_rotation_struct rotation[ ROTATION_COUNT ];
	colour_struct colour;
} shape_struct;

typedef struct
{
	unsigned char shape_count;
	shape_struct *shape;
} shape_set_struct;

typedef struct
{
	unsigned char shape_type;
	unsigned state : 1;
} board_unit_struct;

typedef struct
{
	board_unit_struct *unit;
	unsigned short width, height;
	unsigned short highest_line;
} board_struct;

typedef struct
{
	unsigned char shape_type;
	unsigned char rotation;
	short x, y;
} block_struct;

typedef struct
{
	key_struct key[ GAME_KEY_COUNT ];
	shape_set_struct shape_set;
	board_struct board;
	block_struct current_block, next_block;
	unsigned char playable;
	unsigned char turbo;
	double timer;
	double fall_rate, turbo_rate;
	
	struct _ai_struct *ai;
} game_struct;

typedef struct
{
	unsigned char paused, active, quit;
} system_state_struct;

typedef struct
{
	key_struct key[ GAME_KEY_COUNT ];
	unsigned char playable;
} game_preferences_struct;

unsigned char new_board( board_struct *const board, const unsigned short width, const unsigned short height );
unsigned char copy_board( board_struct *const board_d, const board_struct *const board_s );
void free_board( board_struct *const board );

unsigned char collision_check( const block_struct *const block, const board_struct *const board, const shape_set_struct *const shape_set );
unsigned char rotate_clockwise( block_struct *const block, const board_struct *const board, const shape_set_struct *const shape_set );
unsigned char rotate_counterclockwise( block_struct *const block, const board_struct *const board, const shape_set_struct *const shape_set );
unsigned char move_down( block_struct *const block, const board_struct *const board, const shape_set_struct *const shape_set );
unsigned char move_left( block_struct *const block, const board_struct *const board, const shape_set_struct *const shape_set );
unsigned char move_right( block_struct *const block, const board_struct *const board, const shape_set_struct *const shape_set );
void move_to_bottom( block_struct *const block, const board_struct *const board, const shape_set_struct *const shape_set );
unsigned short check_lines( board_struct *const board );
unsigned short count_holes( const board_struct *const board );
void set_shape( const block_struct *const block, board_struct *const board, const shape_set_struct *const shape_set );

int do_game();

#endif
