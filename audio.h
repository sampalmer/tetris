#ifndef _AUDIO_H_
#define _AUDIO_H_

typedef struct
{
	unsigned char stereo, reverse_stereo;
	
	int frequency;
	unsigned char bps; //Bits per sample.
	unsigned short samples;
} audio_info_struct;

unsigned char audio_init( const audio_info_struct *const info, const unsigned char sample_count );
void audio_deinit();
unsigned char audio_load_sample( const char *file, const unsigned char id );
void audio_free_sample( const unsigned char id, const unsigned char temporary );
unsigned char audio_play_sample( const unsigned char id, const unsigned char volume, const unsigned char pan );
void audio_stop_all_channels();
void audio_pause_all_channels();
void audio_unpause_all_channels();
void audio_free_all_samples( unsigned char temporary );

#endif
