#include <SDL.h>
#include <SDL_opengl.h>
#include <SDL_image.h>

#include "main.h"
#include "video.h"
#include "video_opengl.h"

typedef struct
{
	char *filename;
	GLuint texture;
	unsigned char loaded;
	unsigned char in_use;
} video_texture_struct;

static struct
{
	unsigned char initialised;

	//Video information
	video_info_struct info;
	SDL_Surface *screen;
	
	//Textures.
	video_texture_struct *texture;
	unsigned long texture_count;
} video = { 0 };

inline static unsigned char video_check_errors_opengl()
{
	if( glGetError() )
	{
		//If there's an error, clear all OpenGL errors and return.
		while( glGetError() );
		return TRUE;
	}

	return FALSE;
}

static unsigned char video_set_sdl_opengl_colour_attributes( const unsigned char bpp )
{
	SDL_GL_SetAttribute( SDL_GL_ACCUM_ALPHA_SIZE, 0 );
	switch( bpp )
	{
		case 8:
			SDL_GL_SetAttribute( SDL_GL_RED_SIZE, 3 );
			SDL_GL_SetAttribute( SDL_GL_GREEN_SIZE, 2 );
			SDL_GL_SetAttribute( SDL_GL_BLUE_SIZE, 3 );
			break;
		case 15:
			SDL_GL_SetAttribute( SDL_GL_RED_SIZE, 5 );
			SDL_GL_SetAttribute( SDL_GL_GREEN_SIZE, 5 );
			SDL_GL_SetAttribute( SDL_GL_BLUE_SIZE, 5 );
			break;
		case 16:
			SDL_GL_SetAttribute( SDL_GL_RED_SIZE, 5 );
			SDL_GL_SetAttribute( SDL_GL_GREEN_SIZE, 6 );
			SDL_GL_SetAttribute( SDL_GL_BLUE_SIZE, 5 );
			break;
		case 24:
			SDL_GL_SetAttribute( SDL_GL_RED_SIZE, 8 );
			SDL_GL_SetAttribute( SDL_GL_GREEN_SIZE, 8 );
			SDL_GL_SetAttribute( SDL_GL_BLUE_SIZE, 8 );
			break;
		case 32:
			SDL_GL_SetAttribute( SDL_GL_RED_SIZE, 8 );
			SDL_GL_SetAttribute( SDL_GL_GREEN_SIZE, 8 );
			SDL_GL_SetAttribute( SDL_GL_BLUE_SIZE, 8 );
			break;
		default:
			return FALSE;
	}

	return TRUE;
}

static void video_set_sdl_opengl_palette()
{
	unsigned short i;
	int rbits, gbits, bbits;
	unsigned char rmax, gmax, bmax;
	unsigned char rmask, gmask, bmask;
	unsigned char rshift, gshift, bshift;
	double rmult, gmult, bmult;
	SDL_Color colour[ 256 ];
	
	//Read in the obtained amounts of bits per colour component.
	SDL_GL_GetAttribute( SDL_GL_RED_SIZE, &rbits );
	SDL_GL_GetAttribute( SDL_GL_GREEN_SIZE, &gbits );
	SDL_GL_GetAttribute( SDL_GL_BLUE_SIZE, &bbits );
	
	//Calculate the bit shift values.
	rshift = 8 - rbits;
	gshift = 8 - gbits;
	bshift = 8 - bbits;
	
	//Calculate the maximum values.
	rmax = ( 1 << rbits ) - 1;
	gmax = ( 1 << gbits ) - 1;
	bmax = ( 1 << bbits ) - 1;

	//Calculate the mask values.
	rmask = rmax << rshift;
	gmask = gmax << gshift;
	bmask = bmax << bshift;

	//Calculate the multiply values.
	rmult = 255.0 / ( double )rmax;
	gmult = 255.0 / ( double )gmax;
	bmult = 255.0 / ( double )bmax;
	
	//Calculate the colour values.
	for( i = 0; i < 256; ++i )
	{
		colour[ i ].r = ( double )( ( i & rmask ) >> rshift ) * rmult + 0.5;
		colour[ i ].g = ( double )( ( i & gmask ) >> gshift ) * gmult + 0.5;
		colour[ i ].b = ( double )( ( i & bmask ) >> bshift ) * bmult + 0.5;
	}
	
	//Set the palette.
	SDL_SetPalette( video.screen, SDL_LOGPAL | SDL_PHYSPAL, colour, 0, 256 );
}

static unsigned char video_set_screen_mode_opengl()
{
	unsigned char bpp, newbpp;
	unsigned short width, height;
	Uint32 flags;
	
	//Make sure SDL's video_info subsystem is initialised.
	if( SDL_WasInit( SDL_INIT_VIDEO ) != SDL_INIT_VIDEO )
		if( SDL_InitSubSystem( SDL_INIT_VIDEO ) != 0 )
			return FALSE;
	
	//Work out the amount of bits per pixel to use.
	if( !video.info.fullscreen )
	{
		//Basically, we'll use the current desktop bpp for windowed modes.
		const SDL_VideoInfo *videoinfo;

		videoinfo = SDL_GetVideoInfo();
		bpp = videoinfo->vfmt->BitsPerPixel;
	}
	else
		bpp = video.info.bpp;
	
	//Work out the width and height to use.
	if( !video.info.fullscreen )
	{
		width = video.info.windowed_width;
		height = video.info.windowed_height;
	}
	else
	{
		width = video.info.fullscreen_width;
		height = video.info.fullscreen_height;
	}
	
	//Set the SDL OpenGL attributes.
	SDL_GL_SetAttribute( SDL_GL_DOUBLEBUFFER, 1 );
	SDL_GL_SetAttribute( SDL_GL_DEPTH_SIZE, 0 );
	SDL_GL_SetAttribute( SDL_GL_STENCIL_SIZE, 0 );
	SDL_GL_SetAttribute( SDL_GL_ACCUM_RED_SIZE, 0 );
	SDL_GL_SetAttribute( SDL_GL_ACCUM_GREEN_SIZE, 0 );
	SDL_GL_SetAttribute( SDL_GL_ACCUM_BLUE_SIZE, 0 );
	SDL_GL_SetAttribute( SDL_GL_SWAP_CONTROL, video_info->vsync );
	
	//Set the SDL OpenGL colour-specific attributes.
	if( !video_set_sdl_opengl_colour_attributes( bpp ) )
		return FALSE;
	
	//Set the flags to pass to SDL_SetVideoMode.
	flags = SDL_OPENGL | SDL_ANYFORMAT | SDL_HWSURFACE;
	if( video.info.fullscreen )
		flags |= SDL_FULLSCREEN;
	if( bpp == 8 )
		flags |= SDL_HWPALETTE;

	//Test the video_info mode.
	newbpp = SDL_VideoModeOK( width, height, bpp, flags );
	if( newbpp == 0 )
		return FALSE;
	
	//Check if an alternate amount of bits per pixel was suggested.
	if( newbpp != bpp )
	{
		//If so, use this value instead.
		bpp = newbpp;
		if( !video_set_sdl_opengl_attributes( bpp ) )
			return FALSE;
	}
	
	//Set the video_info mode.
	video.screen = SDL_SetVideoMode( width, height, bpp, flags );
	if( video.screen == NULL )
		return FALSE;

	//If a palettised mode was used, set the palette.
	if( bpp == 8 )
		video_set_sdl_opengl_palette();
	
	//Enable any additional OpenGL capabilities as needed.
	glEnable( GL_TEXTURE_2D );
	glTexEnv( GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_DECAL );
	glShadeModel( GL_FLAT );
	if( video_info->alpha_blending )
	{
		glEnable( GL_BLEND );
		glBlendFunc( GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA );
	}
	glMatrixMode( GL_PROJECTION );
	glLoadIdentity();
	glOrtho( 0.0, ( GLdouble )width, ( GLdouble )height, 0.0, 0.0, 0.0 );
	glMatrixMode( GL_MODELVIEW );
	glLoadIdentity();

	//Check for OpenGL errors.
	if( video_opengl_check_errors() )
		return FALSE;

	return TRUE;
}

unsigned char video_init_opengl( const video_info_struct *const video_info, const unsigned char texture_count )
{
	//Validate.
	if( video.initialised )
		return FALSE;

	//Copy in the video information.
	memcpy( &video.info, video_info, sizeof( video_info_struct ) );

	//Initialise the screen mode and OpenGL.
	if( !video_set_screen_mode_opengl()
		return FALSE;
	
	//Pre-allocate memory for the requested number of textures.
	if( texture_count != 0 )
	{
		video.texture = ( video_texture_struct * )malloc( sizeof( video_texture_struct ) * texture_count );
		if( video.texture == NULL )
		{
			//If this fails, don't worry about allocating memory for samples yet.
			video.texture_count = 0;
		}
		else
		{
			memset( video.texture, 0, sizeof( video_texture_struct ) * texture_count );
			video.texture_count = texture_count;
		}
	}
	else
	{
		video.texture = NULL;
		video.texture_count = 0;
	}
	
	return TRUE;
}

void video_deinit_opengl()
{
	//Validate.
	if( !video.initialised )
		return;

	video_free_all_textures_opengl();
}

unsigned char video_toggle_fullscreen_opengl()
{
	unsigned short current_w, current_h, to_w, to_h;
	
	//Get the source and destination resolution dimensions.
	if( video.info.fullscreen )
	{
		current_w = video.info.fullscreen_width;
		current_h = video.info.fullscreen_height;
		to_w = video.info.windowed_width;
		to_h = video.info.windowed_height;
	}
	else
	{
		current_w = video.info.windowed_width;
		current_h = video.info.windowed_height;
		to_w = video.info.fullscreen_width;
		to_h = video.info.fullscreen_height;
	}
	
	//Toggle the internal fullscreen mode flag.
	video.info.fullscreen = !video.info.fullscreen;

	//If the dimensions are the same, see if an SDL fullscreen toggle can be performed.
	if( current_w == to_w && current_h == to_h )
		if( SDL_WM_ToggleFullScreen( video.screen ) )
			return TRUE;
	
	//Set the video mode again now that the fullscreen flag is toggled.
	if( !video_set_screen_mode_opengl() )
	{
		//If this fails, re-toggle the full-screen flag and set the original video mode again.
		video.info.fullscreen = !video.info.fullscreen;
		if( !video_set_screen_mode_opengl() )
		{
			//If even the original screen mode doesn't work now, we'll have to return FALSE.
			return FALSE;
		}
	}
	
	//Now we'll have to re-load all of the textures again.
	if( !video_reload_all_textures_opengl() )
		return FALSE;
	
	return TRUE;
}

/* Quick utility function for texture creation */
static int power_of_two(int input)
{
	int value = 1;

	while ( value < input ) {
		value <<= 1;
	}
	return value;
}

GLuint SDL_GL_LoadTexture(SDL_Surface *surface, GLfloat *texcoord)
{
	GLuint texture;
	int w, h;
	SDL_Surface *image;
	SDL_Rect area;
	Uint32 saved_flags;
	Uint8  saved_alpha;

	/* Use the surface width and height expanded to powers of 2 */
	w = power_of_two(surface->w);
	h = power_of_two(surface->h);
	texcoord[0] = 0.0f;			/* Min X */
	texcoord[1] = 0.0f;			/* Min Y */
	texcoord[2] = (GLfloat)surface->w / w;	/* Max X */
	texcoord[3] = (GLfloat)surface->h / h;	/* Max Y */

	image = SDL_CreateRGBSurface(
			SDL_SWSURFACE,
			w, h,
			32,
#if SDL_BYTEORDER == SDL_LIL_ENDIAN /* OpenGL RGBA masks */
			0x000000FF, 
			0x0000FF00, 
			0x00FF0000, 
			0xFF000000
#else
			0xFF000000,
			0x00FF0000, 
			0x0000FF00, 
			0x000000FF
#endif
		       );
	if ( image == NULL ) {
		return 0;
	}

	/* Save the alpha blending attributes */
	saved_flags = surface->flags&(SDL_SRCALPHA|SDL_RLEACCELOK);
	saved_alpha = surface->format->alpha;
	if ( (saved_flags & SDL_SRCALPHA) == SDL_SRCALPHA ) {
		SDL_SetAlpha(surface, 0, 0);
	}

	/* Copy the surface into the GL texture image */
	area.x = 0;
	area.y = 0;
	area.w = surface->w;
	area.h = surface->h;
	SDL_BlitSurface(surface, &area, image, &area);

	/* Restore the alpha blending attributes */
	if ( (saved_flags & SDL_SRCALPHA) == SDL_SRCALPHA ) {
		SDL_SetAlpha(surface, saved_flags, saved_alpha);
	}

	/* Create an OpenGL texture for the image */
	glGenTextures(1, &texture);
	glBindTexture(GL_TEXTURE_2D, texture);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexImage2D(GL_TEXTURE_2D,
		     0,
		     GL_RGBA,
		     w, h,
		     0,
		     GL_RGBA,
		     GL_UNSIGNED_BYTE,
		     image->pixels);
	SDL_FreeSurface(image); /* No longer needed */

	return texture;
}

/*
     glTexImage2D(GL_PROXY_TEXTURE_2D, level, internalFormat,  width, height, border, format, type, NULL);  

Note the pixels parameter is NULL, because OpenGL doesn't load texel data when the target parameter is GL_PROXY_TEXTURE_2D. Instead, OpenGL merely considers whether it can accommodate a texture of the specified size and description. If the specified texture can't be accommodated, the width and height texture values will be set to zero. After making a texture proxy call, you'll want to query these values as follows:

    GLint width; glGetTexLevelParameteriv(GL_PROXY_TEXTURE_2D, 0, GL_TEXTURE_WIDTH, &width); if (width==0) Can't use that texture

	GLint texSize; glGetIntegerv(GL_MAX_TEXTURE_SIZE, &texSize);

	SUPPORT OPTIONAL TEXTURE COMPRESSION

	use glPixelStore to choose which portions of the bitmap image to upload as a texture object

	glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP );
	glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP );
	
*/

/* RULES
-If power_of_two( surface->w ) <= MAX_DIM && power_of_two( surface->h ) <= MAX_DIM
	Single texture mode
		w and h = power_of_two of those of the surface
else
	Multiple texture mode
	{
		w/h = surfacew/h % MAX_DIM
	}
-If video.info.subpixel, increase texture dimensions by 1 in both axis. Then increase by another 1 for each axis that has been padded.

*/

unsigned char video_load_texture_opengl( const char *const file, const unsigned long id )
{
	SDL_Surface *image, *surface;
	video_texture_struct *texture;
	unsigned short old_texture_count = video.texture_count;
	int w, h, num_full_subtextures_x, num_full_subtextures_y, last_subtexture_w, last_subtexture_h;
	GLint max_texture_size;

	//Validate the supplied information.
	if( file == NULL )
		return FALSE;
	if( id < video.texture_count )
		if( video.texture_count[ id ].in_use )
			return FALSE;
	
	//Store this machine's OpenGL max texture size.
	glGetIntegerv( GL_MAX_TEXTURE_SIZE, &max_texture_size );
	if( video_opengl_check_errors() )
		return FALSE;
	
	//Attempt to load the file.
	image = IMG_Load( file );
	if( image == NULL )
		return FALSE;
	
	//Store the next power-of-two of the image width and height.
	w = power_of_two( image->w );
	h = power_of_two( image->h );
	
	//Store information about the number of subtextures that the texture
	//will need to be split into.
	num_full_subtextures_x = w / max_texture_size;
	num_full_subtextures_y = h / max_texture_size;
	last_subtexture_w = w % max_texture_size;
	last_subtexture_h = h % max_texture_size;
	
	//The image may need to be padded in either or both dimensions to make the final subtexture have power of two dimensions.
	if( last_subtexture_w > 0 )
		w = num_full_subtextures_x * max_texture_size + power_of_two( last_subtexture_w );
	if( last_subtexture_h > 0 )
		h = num_full_subtextures_y * max_texture_size + power_of_two( last_subtexture_h );
	
	//If subpixel rendering was requested, increase the texture dimensions to make room for a border.
	if( video.info.subpixel )
	{
		++w;
		++h;
		if( last_subtexture_w == 0 )
			++w;
		if( last_subtexture_h == 0 )
			++h;
	}
	
	//Create a new surface of the desired specifications to copy the image onto.
	surface = SDL_CreateRGBSurface(
			SDL_SWSURFACE,
			w, h,
			32,
#if SDL_BYTEORDER == SDL_LIL_ENDIAN /* OpenGL RGBA masks */
			0x000000FF, 
			0x0000FF00, 
			0x00FF0000, 
			0xFF000000
#else
			0xFF000000,
			0x00FF0000, 
			0x0000FF00, 
			0x000000FF
#endif
		       );
	if( surface == NULL )
	{
		SDL_FreeSurface( image );
		return FALSE;
	}

	//Disable alpha blending and colour-keying.
	if( ( image->flags & SDL_SRCALPHA) == SDL_SRCALPHA )
		SDL_SetAlpha(surface, 0, 0);
	SDL_SetColorKey( image, 0, 0 )

	//Perform the copy.
	{
		SDL_Rect dest_rect, source_rect;

		if( video.info.subpixel )
		{
			dest_rect.x = 1;
			dest_rect.y = 1;
		}
		else
		{
			dest_rect.x = 0;
			dest_rect.y = 0;
		}
		dest_rect.w = image->w;
		dest_rect.h = image->h;

		source_rect.x = 0;
		source_rect.y = 0;
		source_rect.w = image->w;
		source_rect.h = image->h;

		if( SDL_BlitSurface( image, &source_rect, surface, &dest_rect ) != 0 )
		{
			SDL_FreeSurface( image );
			SDL_FreeSurface( surface );
			return FALSE;
		}
	}
	
	//Check memory is already allocated for the requested ID.
	if( id >= video.texture_count )
	{
		//If not, reallocate the internal array of samples to make room for the new one.
		if( !video_resize_texture_array( id + 1 ) )
		{
			//If this fails, return.
//FREE IMAGE
			return FALSE;
		}
	}

	//Fill in the texture information.
	texture = video.texture + id;
	texture->filename = _strdup( file );
	if( texture->filename == NULL )
	{
		//If there isn't enough memory to store the file's file name, return.
//FREE IMAGE
		if( video.texture_count != old_texture_count )
			video_resize_texture_array( old_texture_count );
		return FALSE;
	}
//ASSIGN TEXTURE
	texture->loaded = TRUE;
	texture->in_use = TRUE;
	
	return TRUE;
}

unsigned char video_reload_texture_opengl( const unsigned long index )
{
}

void video_free_texture_opengl( const unsigned long index )
{
}

unsigned char video_reload_all_textures_opengl()
{
	video_texture_struct *texture, *last_texture;

	texture = video.texture;
	last_texture = texture + video.texture_count;
	
	//Iterate through each texture, and reload them all.
	for( ; texture < last_texture; ++texture )
		if( texture->in_use )
			if( !video_reload_texture_opengl( texture - video.texture ) )
			{
				video_free_all_textures_opengl();
				return FALSE;
			}

	return TRUE;
}

void video_free_all_textures_opengl()
{
}

void video_blit_texture_opengl( const unsigned long index, const long x, const long y )
{
	//USE:
	//GL_TRIANGLE_STRIP
}

void video_refresh_opengl()
{
}

void video_set_clear_colour_opengl( unsigned char r, unsigned char g, unsigned char b )
{
}

void video_clear_opengl()
{
}
