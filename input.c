#include <stdarg.h>
#include <SDL.h>
#include "main.h"
#include "input.h"

#define MAX_KEYS SDLK_LAST

static struct
{
	unsigned char initialised;

	key_handler_struct key_handler[ MAX_KEYS ];
} input = { 0 };

unsigned char input_init()
{
	//Validate.
	if( input.initialised )
		return FALSE;
	
	//Point each internal key structure to 0.
	memset( input.key_handler, 0, sizeof( key_handler_struct ) * MAX_KEYS );
	
	input.initialised = TRUE;
	
	return TRUE;
}

void input_deinit()
{
	key_handler_struct *key_handler, *last_key_handler;
	
	//Validate.
	if( !input.initialised )
		return;

	key_handler = input.key_handler;
	last_key_handler = key_handler + MAX_KEYS;
	
	//Free any bindings that have not yet been unbound.
	for( ; key_handler < last_key_handler; ++key_handler )
		if( key_handler->binding != NULL )
			free( key_handler->binding );
	
	input.initialised = FALSE;
}

//Initialises a key structure with the given number of bindings.
//(The variable argument list should consist of n pairs of SDLKey followed by SDLMod.
unsigned char input_set_key( key_struct *const key, const unsigned char binding_count, ... )
{
	va_list va;
	key_binding_struct *binding, *last_binding;
	
	//Allocate memory for the requested amount of bindings.
	key->binding = ( key_binding_struct * )malloc( sizeof( key_binding_struct ) * binding_count );
	if( key->binding == NULL && binding_count != 0 )
	{
		memset( key, 0, sizeof( key_struct ) );
		return FALSE;
	}
	
	binding = key->binding;
	last_binding = binding + binding_count;
	
	//Iterate through each requested binding and read in the supplied data.
	va_start( va, binding_count );
	for( ; binding < last_binding; ++binding )
	{
		binding->sym = va_arg( va, SDLKey );
		binding->mod = va_arg( va, SDLMod );
		binding->state = &key->state;
	}
	va_end( va );
	
	//Fill in the key information
	key->binding_count = binding_count;
	memset( &key->state, 0, sizeof( key_state_struct ) );

	return TRUE;
}

//Frees memory associated with a key structure.
void input_free_keys( key_struct *key, const unsigned char key_count )
{
	key_struct *last_key;

	last_key = key + key_count;
	
	//Free the bindings for each key.
	for( ; key < last_key; ++key )
		free( key->binding );
}

//Copies the bindings in a set of keys into another key structure.
unsigned char input_copy_keys( key_struct *const destination_key, const key_struct *source_key, unsigned char key_count )
{
	key_struct *key, *last_key;
	key_binding_struct *source_binding, *binding, *last_binding;
	
	key = destination_key;
	last_key = key + key_count;
	
	//Iterate through each supplied key structure.
	for( ; key < last_key; ++key, ++source_key )
	{
		//Allocate memory for the bindings.
		key->binding = ( key_binding_struct * )malloc( sizeof( key_binding_struct ) * source_key->binding_count );
		if( key->binding == NULL )
		{
			//If this fails, free previous key structures and return.
			input_free_keys( destination_key, key - destination_key );
			return FALSE;
		}
		
		source_binding = source_key->binding;
		binding = key->binding;
		last_binding = binding + source_key->binding_count;
		
		//Iterate through each binding and fill in the details.
		for( ; binding < last_binding; ++binding, ++source_binding )
		{
			binding->sym = source_binding->sym;
			binding->mod = source_binding->mod;
			binding->state = &key->state;
		}

		//Fill in the key information.
		key->binding_count = source_key->binding_count;
		memset( &key->state, 0, sizeof( key_state_struct ) );
	}

	return TRUE;
}

//Locates an already-bound binding.
static key_binding_struct *input_get_binding( key_handler_struct *key_handler, const key_binding_struct *const binding )
{
	key_binding_struct *current_binding, *last_binding;

	current_binding = key_handler->binding;
	last_binding = current_binding + key_handler->binding_count;

	//Iterate through the existing bindings.
	for( ; current_binding < last_binding; ++current_binding )
		if( memcmp( current_binding, binding, sizeof( key_binding_struct ) ) == 0 )
			return current_binding;

	return NULL;
}

//Adds a binding.
static unsigned char input_bind( const key_binding_struct *const binding )
{
	key_handler_struct *key_handler;
	key_binding_struct *previous_binding;

	//Get a pointer to the key handler for the key this binding is for.
	key_handler = input.key_handler + binding->sym;
	
	//Make sure that the given binding is not already added.
	if( input_get_binding( key_handler, binding ) != NULL )
		return FALSE;
	
	//Increase the size of the array to add this binding.
	key_handler->binding_count += 1;
	previous_binding = key_handler->binding;
	key_handler->binding = ( key_binding_struct * )realloc( previous_binding, sizeof( key_binding_struct ) * key_handler->binding_count );
	if( key_handler->binding == NULL )
	{
		key_handler->binding = previous_binding;
		key_handler->binding_count -= 1;
		return FALSE;
	}

	//Copy the new binding in.
	memcpy( key_handler->binding + key_handler->binding_count - 1, binding, sizeof( key_binding_struct ) );

	return TRUE;
}

//Removes a binding.
static void input_unbind( key_binding_struct *binding )
{
	key_handler_struct *key_handler;
	key_binding_struct *last_binding;

	//Get a pointer to the key handler for the key this binding is for.
	key_handler = input.key_handler + binding->sym;

	//Get a pointer to the copy of the given binding in the key handler structure.
	binding = input_get_binding( key_handler, binding );
	if( binding == NULL )
		return;

	last_binding = key_handler->binding + key_handler->binding_count - 1;

	//Remove the binding.
	for( ; binding < last_binding; ++binding )
		memcpy( binding, binding + 1, sizeof( key_binding_struct ) );

	//Decrease the size of the array to remove this binding.
	key_handler->binding_count -= 1;
	last_binding = key_handler->binding;
	key_handler->binding = ( key_binding_struct * )realloc( last_binding, sizeof( key_binding_struct ) * key_handler->binding_count );
	if( key_handler->binding == NULL && key_handler->binding_count != 0 )
		key_handler->binding = last_binding;
}

//Adds all bindings in an array of key structures.
unsigned char input_register_keys( const key_struct *key, const unsigned char key_count )
{
	const key_struct *last_key;
	key_binding_struct *binding, *last_binding;

	last_key = key + key_count;
	
	//Iterate through each supplied key structure.
	for( ; key < last_key; ++key )
	{
		binding = key->binding;
		last_binding = binding + key->binding_count;
		
		//Iterate through each binding in the key structure.
		for( ; binding < last_binding; ++binding )
			//Add the binding.
			if( !input_bind( binding ) )
			{
				input_unregister_keys( key, key_count );
				return FALSE;
			}
	}

	return TRUE;
}

//Removes all bindings in an array of key structures.
void input_unregister_keys( const key_struct *key, const unsigned char key_count )
{
	const key_struct *last_key;
	key_binding_struct *binding, *last_binding;

	last_key = key + key_count;
	
	//Iterate through each supplied key structure.
	for( ; key < last_key; ++key )
	{
		binding = key->binding;
		last_binding = binding + key->binding_count;
		
		//Iterate through each binding in the key structure.
		for( ; binding < last_binding; ++binding )
			//Remove the binding.
			input_unbind( binding );
	}
}

void input_process_key_event( SDL_KeyboardEvent *event )
{
	key_handler_struct *key_handler;
	key_binding_struct *binding, *last_binding;
	key_state_struct *state;
	SDLMod mod;
	
	//Ignore caps-lock and num-lock states.
	mod = event->keysym.mod;
	mod &= ~( KMOD_CAPS | KMOD_NUM );
	
	//Get a pointer to the key handler for this key.
	key_handler = input.key_handler + event->keysym.sym;

	binding = key_handler->binding;
	last_binding = binding + key_handler->binding_count;
	
	//Iterate through each binding in the key handler.
	for( ; binding < last_binding; ++binding )
		if( ( mod & binding->mod ) == binding->mod )
		{
			state = binding->state;

			if( event->state == SDL_PRESSED )
			{
				state->pressed = TRUE;
				state->down = TRUE;
				state->released = FALSE;
			}
			else
			{
				state->pressed = FALSE;
				state->down = FALSE;
				state->released = TRUE;
			}
		}
}
