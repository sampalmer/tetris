#include <stdlib.h>
#include <SDL.h>
#include "main.h"
#include "game.h"
#include "ai.h"
#include "input.h"
#include "video.h"
#include "audio.h"

static void free_video( video_struct *const video )
{
	video_free_all_textures( video );
	shut_down_video();
}

static unsigned char setup_video( video_struct *const video )
{
	//Set the display mode.
	if( !set_display_mode( &video->preferences, &video->info ) )
		return FALSE;

	//Set up room to store each texture.
	video->texture_count = TEXTURE_COUNT;
	video->texture = ( video_texture_struct ** )malloc( sizeof( video_texture_struct * ) * video->texture_count );
	if( video->texture == NULL )
	{
		video->texture_count = 0;
		shut_down_video();
		return FALSE;
	}
	memset( video->texture, 0, sizeof( video_texture_struct * ) * video->texture_count );

	//Load the textures.
	video->texture[ TEXTURE_BLOCK ] = video_load_texture( "TetrisBlock16.png", 0 );
	video->texture[ TEXTURE_HBORDER ] = video_load_texture( "hborder.png", 0 );
	video->texture[ TEXTURE_LBORDER ] = video_load_texture( "lborder.png", 0 );
	video->texture[ TEXTURE_RBORDER ] = video_load_texture( "rborder.png", 0 );
	video->texture[ TEXTURE_BACKGROUND ] = video_load_texture( "Background.png", 0 );

	//Make sure that all of the textures loaded.
	{
		unsigned char i;

		for( i = 0; i < TEXTURE_COUNT; ++i )
			if( video->texture[ i ] == NULL )
			{
				//If not, release resources and fail.
				free_video( video );
				return FALSE;
			}
	}

	return TRUE;
}

static void free_shape( shape_struct *const shape )
{
	unsigned char i;

	for( i = 0; i < ROTATION_COUNT; ++i )
	{
		free( shape->rotation[ i ].array );
		memset( shape->rotation + i, 0, sizeof( shape_rotation_struct ) );
	}
}

static unsigned char generate_standard_shape_T( shape_struct *const shape )
{
	shape_rotation_struct *current_rotation;

	//Colour
	shape->colour.r = 1.0;
	shape->colour.g = 0.5;
	shape->colour.b = 0.0;

	//Upright
	current_rotation = shape->rotation;
	current_rotation->width = 3;
	current_rotation->height = 2;
	current_rotation->array = ( unsigned char * )malloc( sizeof( unsigned char ) * current_rotation->width * current_rotation->height );
	if( current_rotation->array == NULL )
		return FALSE;
	memcpy( current_rotation->array, "\0\1\0"
                                     "\1\1\1", sizeof( unsigned char ) * current_rotation->width * current_rotation->height );
	current_rotation->offsetx = 0;
	current_rotation->offsety = 0;

	//Right side
	current_rotation = shape->rotation + 1;
	current_rotation->width = 2;
	current_rotation->height = 3;
	current_rotation->array = ( unsigned char * )malloc( sizeof( unsigned char ) * current_rotation->width * current_rotation->height );
	if( current_rotation->array == NULL )
	{
		free( shape->rotation->array );
		return FALSE;
	}
	memcpy( current_rotation->array, "\1\0"
	                                 "\1\1"
	                                 "\1\0", sizeof( unsigned char ) * current_rotation->width * current_rotation->height );
	current_rotation->offsetx = -1;
	current_rotation->offsety = 0;

	//Upside-down
	current_rotation = shape->rotation + 2;
	current_rotation->width = 3;
	current_rotation->height = 2;
	current_rotation->array = ( unsigned char * )malloc( sizeof( unsigned char ) * current_rotation->width * current_rotation->height );
	if( current_rotation->array == NULL )
	{
		free( shape->rotation->array );
		free( shape->rotation[ 1 ].array );
		return FALSE;
	}
	memcpy( current_rotation->array, "\1\1\1"
	                                 "\0\1\0", sizeof( unsigned char ) * current_rotation->width * current_rotation->height );
	current_rotation->offsetx = 1;
	current_rotation->offsety = -1;

	//Left side
	current_rotation = shape->rotation + 3;
	current_rotation->width = 2;
	current_rotation->height = 3;
	current_rotation->array = ( unsigned char * )malloc( sizeof( unsigned char ) * current_rotation->width * current_rotation->height );
	if( current_rotation->array == NULL )
	{
		free( shape->rotation->array );
		free( shape->rotation[ 1 ].array );
		free( shape->rotation[ 2 ].array );
		return FALSE;
	}
	memcpy( current_rotation->array, "\0\1"
	                                 "\1\1"
	                                 "\0\1", sizeof( unsigned char ) * current_rotation->width * current_rotation->height );
	current_rotation->offsetx = 0;
	current_rotation->offsety = 1;

	return TRUE;
}

static unsigned char generate_standard_shape_I( shape_struct *const shape )
{
	shape_rotation_struct *current_rotation;

	//Colour
	shape->colour.r = 1.0;
	shape->colour.g = 1.0;
	shape->colour.b = 0.0;

	//Upright
	current_rotation = shape->rotation;
	current_rotation->width = 4;
	current_rotation->height = 1;
	current_rotation->array = ( unsigned char * )malloc( sizeof( unsigned char ) * current_rotation->width * current_rotation->height );
	if( current_rotation->array == NULL )
		return FALSE;
	memcpy( current_rotation->array, "\1\1\1\1", sizeof( unsigned char ) * current_rotation->width * current_rotation->height );
	current_rotation->offsetx = 1;
	current_rotation->offsety = -1;

	//Right side
	current_rotation = shape->rotation + 1;
	current_rotation->width = 1;
	current_rotation->height = 4;
	current_rotation->array = ( unsigned char * )malloc( sizeof( unsigned char ) * current_rotation->width * current_rotation->height );
	if( current_rotation->array == NULL )
	{
		free( shape->rotation->array );
		return FALSE;
	}
	memcpy( current_rotation->array, "\1"
	                                 "\1"
	                                 "\1"
	                                 "\1", sizeof( unsigned char ) * current_rotation->width * current_rotation->height );
	current_rotation->offsetx = -2;
	current_rotation->offsety = 1;

	//Upside-down
	current_rotation = shape->rotation + 2;
	current_rotation->width = 4;
	current_rotation->height = 1;
	current_rotation->array = ( unsigned char * )malloc( sizeof( unsigned char ) * current_rotation->width * current_rotation->height );
	if( current_rotation->array == NULL )
	{
		free( shape->rotation->array );
		free( shape->rotation[ 1 ].array );
		return FALSE;
	}
	memcpy( current_rotation->array, "\1\1\1\1", sizeof( unsigned char ) * current_rotation->width * current_rotation->height );
	current_rotation->offsetx = 2;
	current_rotation->offsety = -2;

	//Left side
	current_rotation = shape->rotation + 3;
	current_rotation->width = 1;
	current_rotation->height = 4;
	current_rotation->array = ( unsigned char * )malloc( sizeof( unsigned char ) * current_rotation->width * current_rotation->height );
	if( current_rotation->array == NULL )
	{
		free( shape->rotation->array );
		free( shape->rotation[ 1 ].array );
		free( shape->rotation[ 2 ].array );
		return FALSE;
	}
	memcpy( current_rotation->array, "\1"
	                                 "\1"
	                                 "\1"
	                                 "\1", sizeof( unsigned char ) * current_rotation->width * current_rotation->height );
	current_rotation->offsetx = -1;
	current_rotation->offsety = 2;

	return TRUE;
}

static unsigned char generate_standard_shape_S( shape_struct *const shape )
{
	shape_rotation_struct *current_rotation;

	//Colour
	shape->colour.r = 1.0;
	shape->colour.g = 0.0;
	shape->colour.b = 0.5;

	//Upright
	current_rotation = shape->rotation;
	current_rotation->width = 3;
	current_rotation->height = 2;
	current_rotation->array = ( unsigned char * )malloc( sizeof( unsigned char ) * current_rotation->width * current_rotation->height );
	if( current_rotation->array == NULL )
		return FALSE;
	memcpy( current_rotation->array, "\0\1\1"
	                                 "\1\1\0", sizeof( unsigned char ) * current_rotation->width * current_rotation->height );
	current_rotation->offsetx = 0;
	current_rotation->offsety = 0;

	//Right side
	current_rotation = shape->rotation + 1;
	current_rotation->width = 2;
	current_rotation->height = 3;
	current_rotation->array = ( unsigned char * )malloc( sizeof( unsigned char ) * current_rotation->width * current_rotation->height );
	if( current_rotation->array == NULL )
	{
		free( shape->rotation->array );
		return FALSE;
	}
	memcpy( current_rotation->array, "\1\0"
	                                 "\1\1"
	                                 "\0\1", sizeof( unsigned char ) * current_rotation->width * current_rotation->height );
	current_rotation->offsetx = -1;
	current_rotation->offsety = 0;

	//Upside-down
	current_rotation = shape->rotation + 2;
	current_rotation->width = 3;
	current_rotation->height = 2;
	current_rotation->array = ( unsigned char * )malloc( sizeof( unsigned char ) * current_rotation->width * current_rotation->height );
	if( current_rotation->array == NULL )
	{
		free( shape->rotation->array );
		free( shape->rotation[ 1 ].array );
		return FALSE;
	}
	memcpy( current_rotation->array, "\0\1\1"
	                                 "\1\1\0", sizeof( unsigned char ) * current_rotation->width * current_rotation->height );
	current_rotation->offsetx = 1;
	current_rotation->offsety = -1;

	//Left side
	current_rotation = shape->rotation + 3;
	current_rotation->width = 2;
	current_rotation->height = 3;
	current_rotation->array = ( unsigned char * )malloc( sizeof( unsigned char ) * current_rotation->width * current_rotation->height );
	if( current_rotation->array == NULL )
	{
		free( shape->rotation->array );
		free( shape->rotation[ 1 ].array );
		free( shape->rotation[ 2 ].array );
		return FALSE;
	}
	memcpy( current_rotation->array, "\1\0"
	                                 "\1\1"
	                                 "\0\1", sizeof( unsigned char ) * current_rotation->width * current_rotation->height );
	current_rotation->offsetx = 0;
	current_rotation->offsety = 1;

	return TRUE;
}

static unsigned char generate_standard_shape_Z( shape_struct *const shape )
{
	shape_rotation_struct *current_rotation;

	//Colour
	shape->colour.r = 0.0;
	shape->colour.g = 1.0;
	shape->colour.b = 0.0;

	//Upright
	current_rotation = shape->rotation;
	current_rotation->width = 3;
	current_rotation->height = 2;
	current_rotation->array = ( unsigned char * )malloc( sizeof( unsigned char ) * current_rotation->width * current_rotation->height );
	if( current_rotation->array == NULL )
		return FALSE;
	memcpy( current_rotation->array, "\1\1\0"
	                                 "\0\1\1", sizeof( unsigned char ) * current_rotation->width * current_rotation->height );
	current_rotation->offsetx = 0;
	current_rotation->offsety = 0;

	//Right side
	current_rotation = shape->rotation + 1;
	current_rotation->width = 2;
	current_rotation->height = 3;
	current_rotation->array = ( unsigned char * )malloc( sizeof( unsigned char ) * current_rotation->width * current_rotation->height );
	if( current_rotation->array == NULL )
	{
		free( shape->rotation->array );
		return FALSE;
	}
	memcpy( current_rotation->array, "\0\1"
	                                 "\1\1"
	                                 "\1\0", sizeof( unsigned char ) * current_rotation->width * current_rotation->height );
	current_rotation->offsetx = -1;
	current_rotation->offsety = 0;

	//Upside-down
	current_rotation = shape->rotation + 2;
	current_rotation->width = 3;
	current_rotation->height = 2;
	current_rotation->array = ( unsigned char * )malloc( sizeof( unsigned char ) * current_rotation->width * current_rotation->height );
	if( current_rotation->array == NULL )
	{
		free( shape->rotation->array );
		free( shape->rotation[ 1 ].array );
		return FALSE;
	}
	memcpy( current_rotation->array, "\1\1\0"
	                                 "\0\1\1", sizeof( unsigned char ) * current_rotation->width * current_rotation->height );
	current_rotation->offsetx = 1;
	current_rotation->offsety = -1;

	//Left side
	current_rotation = shape->rotation + 3;
	current_rotation->width = 2;
	current_rotation->height = 3;
	current_rotation->array = ( unsigned char * )malloc( sizeof( unsigned char ) * current_rotation->width * current_rotation->height );
	if( current_rotation->array == NULL )
	{
		free( shape->rotation->array );
		free( shape->rotation[ 1 ].array );
		free( shape->rotation[ 2 ].array );
		return FALSE;
	}
	memcpy( current_rotation->array, "\0\1"
	                                 "\1\1"
	                                 "\1\0", sizeof( unsigned char ) * current_rotation->width * current_rotation->height );
	current_rotation->offsetx = 0;
	current_rotation->offsety = 1;

	return TRUE;
}

static unsigned char generate_standard_shape_L( shape_struct *const shape )
{
	shape_rotation_struct *current_rotation;

	//Colour
	shape->colour.r = 0.0;
	shape->colour.g = 0.0;
	shape->colour.b = 1.0;

	//Upright
	current_rotation = shape->rotation;
	current_rotation->width = 3;
	current_rotation->height = 2;
	current_rotation->array = ( unsigned char * )malloc( sizeof( unsigned char ) * current_rotation->width * current_rotation->height );
	if( current_rotation->array == NULL )
		return FALSE;
	memcpy( current_rotation->array, "\0\0\1"
	                                 "\1\1\1", sizeof( unsigned char ) * current_rotation->width * current_rotation->height );
	current_rotation->offsetx = 0;
	current_rotation->offsety = 0;

	//Right side
	current_rotation = shape->rotation + 1;
	current_rotation->width = 2;
	current_rotation->height = 3;
	current_rotation->array = ( unsigned char * )malloc( sizeof( unsigned char ) * current_rotation->width * current_rotation->height );
	if( current_rotation->array == NULL )
	{
		free( shape->rotation->array );
		return FALSE;
	}
	memcpy( current_rotation->array, "\1\0"
	                                 "\1\0"
	                                 "\1\1", sizeof( unsigned char ) * current_rotation->width * current_rotation->height );
	current_rotation->offsetx = -1;
	current_rotation->offsety = 0;

	//Upside-down
	current_rotation = shape->rotation + 2;
	current_rotation->width = 3;
	current_rotation->height = 2;
	current_rotation->array = ( unsigned char * )malloc( sizeof( unsigned char ) * current_rotation->width * current_rotation->height );
	if( current_rotation->array == NULL )
	{
		free( shape->rotation->array );
		free( shape->rotation[ 1 ].array );
		return FALSE;
	}
	memcpy( current_rotation->array, "\1\1\1"
	                                 "\1\0\0", sizeof( unsigned char ) * current_rotation->width * current_rotation->height );
	current_rotation->offsetx = 1;
	current_rotation->offsety = -1;

	//Left side
	current_rotation = shape->rotation + 3;
	current_rotation->width = 2;
	current_rotation->height = 3;
	current_rotation->array = ( unsigned char * )malloc( sizeof( unsigned char ) * current_rotation->width * current_rotation->height );
	if( current_rotation->array == NULL )
	{
		free( shape->rotation->array );
		free( shape->rotation[ 1 ].array );
		free( shape->rotation[ 2 ].array );
		return FALSE;
	}
	memcpy( current_rotation->array, "\1\1"
	                                 "\0\1"
	                                 "\0\1", sizeof( unsigned char ) * current_rotation->width * current_rotation->height );
	current_rotation->offsetx = 0;
	current_rotation->offsety = 1;

	return TRUE;
}

static unsigned char generate_standard_shape_J( shape_struct *const shape )
{
	shape_rotation_struct *current_rotation;

	//Colour
	shape->colour.r = 0.0;
	shape->colour.g = 1.0;
	shape->colour.b = 1.0;

	//Upright
	current_rotation = shape->rotation;
	current_rotation->width = 3;
	current_rotation->height = 2;
	current_rotation->array = ( unsigned char * )malloc( sizeof( unsigned char ) * current_rotation->width * current_rotation->height );
	if( current_rotation->array == NULL )
	{
		free( shape->rotation->array );
		return FALSE;
	}
	memcpy( current_rotation->array, "\1\0\0"
	                                 "\1\1\1", sizeof( unsigned char ) * current_rotation->width * current_rotation->height );
	current_rotation->offsetx = 0;
	current_rotation->offsety = 0;

	//Right side
	current_rotation = shape->rotation + 1;
	current_rotation->width = 2;
	current_rotation->height = 3;
	current_rotation->array = ( unsigned char * )malloc( sizeof( unsigned char ) * current_rotation->width * current_rotation->height );
	if( current_rotation->array == NULL )
	{
		free( shape->rotation->array );
		return FALSE;
	}
	memcpy( current_rotation->array, "\1\1"
	                                 "\1\0"
	                                 "\1\0", sizeof( unsigned char ) * current_rotation->width * current_rotation->height );
	current_rotation->offsetx = -1;
	current_rotation->offsety = 0;

	//Upside-down
	current_rotation = shape->rotation + 2;
	current_rotation->width = 3;
	current_rotation->height = 2;
	current_rotation->array = ( unsigned char * )malloc( sizeof( unsigned char ) * current_rotation->width * current_rotation->height );
	if( current_rotation->array == NULL )
	{
		free( shape->rotation->array );
		free( shape->rotation[ 1 ].array );
		return FALSE;
	}
	memcpy( current_rotation->array, "\1\1\1"
	                                 "\0\0\1", sizeof( unsigned char ) * current_rotation->width * current_rotation->height );
	current_rotation->offsetx = 1;
	current_rotation->offsety = -1;

	//Left side
	current_rotation = shape->rotation + 3;
	current_rotation->width = 2;
	current_rotation->height = 3;
	current_rotation->array = ( unsigned char * )malloc( sizeof( unsigned char ) * current_rotation->width * current_rotation->height );

	if( current_rotation->array == NULL )
	{
		free( shape->rotation->array );
		free( shape->rotation[ 1 ].array );
		free( shape->rotation[ 2 ].array );
		return FALSE;
	}
	memcpy( current_rotation->array, "\0\1"
	                                 "\0\1"
	                                 "\1\1", sizeof( unsigned char ) * current_rotation->width * current_rotation->height );
	current_rotation->offsetx = 0;
	current_rotation->offsety = 1;

	return TRUE;
}

static unsigned char generate_standard_shape_D( shape_struct *const shape )
{
	shape_rotation_struct *current_rotation;

	//Colour
	shape->colour.r = 1.0;
	shape->colour.g = 0.0;
	shape->colour.b = 0.0;

	//Upright
	current_rotation = shape->rotation;
	current_rotation->width = 2;
	current_rotation->height = 2;
	current_rotation->array = ( unsigned char * )malloc( sizeof( unsigned char ) * current_rotation->width * current_rotation->height );
	if( current_rotation->array == NULL )
		return FALSE;
	memcpy( current_rotation->array, "\1\1"
	                                 "\1\1", sizeof( unsigned char ) * current_rotation->width * current_rotation->height );
	current_rotation->offsetx = 0;
	current_rotation->offsety = 0;

	//Right side
	current_rotation = shape->rotation + 1;
	current_rotation->width = 2;
	current_rotation->height = 2;
	current_rotation->array = ( unsigned char * )malloc( sizeof( unsigned char ) * current_rotation->width * current_rotation->height );
	if( current_rotation->array == NULL )
	{
		free( shape->rotation->array );
		return FALSE;
	}
	memcpy( current_rotation->array, "\1\1"
	                                 "\1\1", sizeof( unsigned char ) * current_rotation->width * current_rotation->height );
	current_rotation->offsetx = 0;
	current_rotation->offsety = 0;

	//Upside-down
	current_rotation = shape->rotation + 2;
	current_rotation->width = 2;
	current_rotation->height = 2;
	current_rotation->array = ( unsigned char * )malloc( sizeof( unsigned char ) * current_rotation->width * current_rotation->height );
	if( current_rotation->array == NULL )
	{
		free( shape->rotation->array );
		free( shape->rotation[ 1 ].array );
		return FALSE;
	}
	memcpy( current_rotation->array, "\1\1"
	                                 "\1\1", sizeof( unsigned char ) * current_rotation->width * current_rotation->height );
	current_rotation->offsetx = 0;
	current_rotation->offsety = 0;

	//Left side
	current_rotation = shape->rotation + 3;
	current_rotation->width = 2;
	current_rotation->height = 2;
	current_rotation->array = ( unsigned char * )malloc( sizeof( unsigned char ) * current_rotation->width * current_rotation->height );
	if( current_rotation->array == NULL )
	{
		free( shape->rotation->array );
		free( shape->rotation[ 1 ].array );
		free( shape->rotation[ 2 ].array );
		return FALSE;
	}
	memcpy( current_rotation->array, "\1\1"
	                                 "\1\1", sizeof( unsigned char ) * current_rotation->width * current_rotation->height );
	current_rotation->offsetx = 0;
	current_rotation->offsety = 0;

	return TRUE;
}

static unsigned char generate_standard_shape( shape_struct *const shape, const unsigned char shape_index )
{
	switch( shape_index )
	{
		case SHAPE_T:
			return generate_standard_shape_T( shape );
		case SHAPE_I:
			return generate_standard_shape_I( shape );
		case SHAPE_S:
			return generate_standard_shape_S( shape );
		case SHAPE_Z:
			return generate_standard_shape_Z( shape );
		case SHAPE_L:
			return generate_standard_shape_L( shape );
		case SHAPE_J:
			return generate_standard_shape_J( shape );
		case SHAPE_D:
			return generate_standard_shape_D( shape );
		default:
			return FALSE;
	}
}

static unsigned char generate_standard_shape_set( shape_set_struct *const shape_set )
{
	unsigned char i, j;

	shape_set->shape_count = SHAPE_COUNT;

	//Allocate memory for each shape.
	shape_set->shape = ( shape_struct * )malloc( sizeof( shape_struct ) * shape_set->shape_count );
	if( shape_set->shape == NULL )
		return FALSE;

	//Iterate through each shape type and generate it.
	for( i = 0; i < shape_set->shape_count; ++i )
		if( !generate_standard_shape( shape_set->shape + i, i ) )
		{
			//If a shape cannot be created, free the previous ones and terminate generation of shapes.
			for( j = 0; j < i; ++j )
				free_shape( shape_set->shape + j );
			break;
		}

	//If the previous loop did not complete, assume failure.
	if( i < shape_set->shape_count )
	{
		free( shape_set->shape );
		return FALSE;
	}

	return TRUE;
}

static unsigned char new_shape_set( shape_set_struct *const shape_set )
{
	if( !generate_standard_shape_set( shape_set ) )
		return FALSE;

	return TRUE;
}

static void free_shape_set( shape_set_struct *const shape_set )
{
	unsigned char i;

	for( i = 0; i < shape_set->shape_count; ++i )
		free_shape( shape_set->shape + i );

	free( shape_set->shape );
	shape_set->shape_count = 0;
	shape_set->shape = NULL;
}

unsigned char new_board( board_struct *const board, const unsigned short width, const unsigned short height )
{
	board->width = width;
	board->height = height;
	board->highest_line = height;
	board->unit = ( board_unit_struct * )malloc( sizeof( board_unit_struct ) * height * width );

	if( board->unit == NULL )
		return FALSE;

	memset( board->unit, 0, sizeof( board_unit_struct ) * height * width );

	return TRUE;
}

unsigned char copy_board( board_struct *const board_d, const board_struct *const board_s )
{
	//Make sure the boards are of equal dimensions.
	if( board_d->width != board_s->width || board_d->height != board_s->height )
		return FALSE;

	//Perform the copy.
	memcpy( board_d->unit, board_s->unit, sizeof( board_unit_struct ) * board_s->width * board_s->height );
	board_d->highest_line = board_s->highest_line;

	return TRUE;
}

void free_board( board_struct *const board )
{
	free( board->unit );
	board->width = 0;
	board->height = 0;
	board->highest_line = 0;
	board->unit = NULL;
}

//Randomly generates a Tetris block and resets its position.
static void new_block( block_struct *const block, const board_struct *const board, const shape_set_struct *const shape_set )
{
	block->shape_type = rand() % shape_set->shape_count;
	block->rotation = 0;
	block->y = 0;
	block->x = ( board->width - shape_set->shape[ block->shape_type ].rotation[ block->rotation ].width ) / 2;
}

static unsigned char new_game_keys( key_struct *const key, const game_preferences_struct *const game_preferences )
{
	//Copy the key binding definitions from the supplied preferences.
	if( !input_copy_keys( key, game_preferences->key, GAME_KEY_COUNT ) )
		return FALSE;
	
	//Register the keys.
	if( !input_register_keys( key, GAME_KEY_COUNT ) )
	{
		input_free_keys( key, GAME_KEY_COUNT );
		return FALSE;
	}

	return TRUE;
}

static void free_keys( key_struct *const key )
{
	input_unregister_keys( key, GAME_KEY_COUNT );
	input_free_keys( key, GAME_KEY_COUNT );
}

static unsigned char new_ai( ai_struct **ai, const game_preferences_struct *const game_preferences )
{
	if( game_preferences->playable )
		*ai = NULL;
	else
	{
		*ai = ( ai_struct * )malloc( sizeof( ai_struct ) );
		if( *ai == NULL )
			return FALSE;
		memset( *ai, 0, sizeof( ai_struct ) );
	}

	return TRUE;
}

static void free_ai( ai_struct **ai )
{
	if( *ai == NULL )
		return;

	ai_process_pathfind_free_node_list( ( *ai )->path );
	*ai = NULL;
}

static unsigned char new_game( game_struct *const game, const game_preferences_struct *const game_preferences, const unsigned int seed )
{
	srand( seed );

	if( !new_shape_set( &game->shape_set ) )
		return FALSE;

	if( !new_board( &game->board, 10, 20 ) )
	{
		free_shape_set( &game->shape_set );
		return FALSE;
	}

	if( !new_game_keys( game->key, game_preferences ) )
	{
		free_board( &game->board );
		free_shape_set( &game->shape_set );
		return FALSE;
	}

	if( !new_ai( &game->ai, game_preferences ) )
	{
		free_board( &game->board );
		free_shape_set( &game->shape_set );
		free_keys( game->key );
		return FALSE;
	}

	new_block( &game->current_block, &game->board, &game->shape_set );
	new_block( &game->next_block, &game->board, &game->shape_set );

	//If this is a computer-controlled game,
	//alert the AI that a new block was created.
	if( !game_preferences->playable )
		ai_new_block( game );

	//Apply game preferences.
	game->fall_rate = 1.0;
	game->turbo_rate = 0.05;
	game->turbo = 0;
	game->timer = 0.0;
	game->playable = game_preferences->playable;

	return TRUE;
}

static void free_game( game_struct *const game )
{
	free_shape_set( &game->shape_set );
	free_board( &game->board );
	free_keys( game->key );
	free_ai( &game->ai );
}

unsigned char collision_check( const block_struct *const block, const board_struct *const board, const shape_set_struct *const shape_set )
{
	const shape_rotation_struct *rotation;
	unsigned char x, y;

	rotation = shape_set->shape[ block->shape_type ].rotation + block->rotation;

	for( y = 0; y < rotation->height; ++y )
		for( x = 0; x < rotation->width; ++x )
		{
			//Make sure that the current unit of the block is not transparent.
			if( rotation->array[ y * rotation->width + x ] == 0 )
				continue;

			//Make sure that the current unit of the block is still within the grid.
			if( block->y + y < 0 || block->y + y >= board->height || block->x + x < 0 || block->x + x >= board->width )
				return TRUE;

			//Make sure that this position in the grid is not occupied.
			if( board->unit[ ( block->y + y ) * board->width + block->x + x ].state == 1 )
				return TRUE;
		}

	return FALSE;
}

unsigned char rotate_clockwise( block_struct *const block, const board_struct *const board, const shape_set_struct *const shape_set )
{
	const shape_rotation_struct *rotation;
	unsigned char current_rotation, current_x, current_y;

	//Store a pointer to the current block's shape/rotation.
	rotation = shape_set->shape[ block->shape_type ].rotation + block->rotation;

	//Store the current state of the given block.
	current_rotation = block->rotation;
	current_x = ( unsigned char )block->x;
	current_y = ( unsigned char )block->y;

	//Rotate the block clockwise.
	block->rotation += 1;
	while( block->rotation >= ROTATION_COUNT )
		block->rotation -= ROTATION_COUNT;

	rotation = shape_set->shape[ block->shape_type ].rotation + block->rotation;
	block->x -= rotation->offsetx;
	block->y -= rotation->offsety;

	//Perform a collision check.
	if( collision_check( block, board, shape_set ) )
	{
		//If it fails, revert the block to its previous state and return.
		block->rotation = current_rotation;
		block->x = current_x;
		block->y = current_y;
		return FALSE;
	}

	return TRUE;
}

unsigned char rotate_counterclockwise( block_struct *const block, const board_struct *const board, const shape_set_struct *const shape_set )
{
	const shape_rotation_struct *rotation;
	unsigned char current_rotation, current_x, current_y;

	//Store a pointer to the current block's shape/rotation.
	rotation = shape_set->shape[ block->shape_type ].rotation + block->rotation;

	//Store the current state of the given block.
	current_rotation = block->rotation;
	current_x = ( unsigned char )block->x;
	current_y = ( unsigned char )block->y;

	//Rotate the block counterclockwise.
	if( block->rotation == 0 )
		block->rotation = ROTATION_COUNT - 1;
	else
		block->rotation -= 1;

	block->x += rotation->offsetx;
	block->y += rotation->offsety;
	rotation = shape_set->shape[ block->shape_type ].rotation + block->rotation;

	//Perform a collision check.
	if( collision_check( block, board, shape_set ) )
	{
		//If it fails, revert the block to its previous state and return.
		block->rotation = current_rotation;
		block->x = current_x;
		block->y = current_y;
		return FALSE;
	}

	return TRUE;
}

unsigned char move_down( block_struct *const block, const board_struct *const board, const shape_set_struct *const shape_set )
{
	const shape_rotation_struct *rotation;
	unsigned char current_y;

	//Store a pointer to the current block's shape/rotation.
	rotation = shape_set->shape[ block->shape_type ].rotation + block->rotation;

	//Store the current state of the given block.
	current_y = ( unsigned char )block->y;

	//Move the block down.
	block->y += 1;

	//Perform a collision check.
	if( collision_check( block, board, shape_set ) )
	{
		//If it fails, revert the block to its previous state and return.
		block->y = current_y;
		return FALSE;
	}

	return TRUE;
}

unsigned char move_left( block_struct *const block, const board_struct *const board, const shape_set_struct *const shape_set )
{
	const shape_rotation_struct *rotation;
	unsigned char current_x;

	//Store a pointer to the current block's shape/rotation.
	rotation = shape_set->shape[ block->shape_type ].rotation + block->rotation;

	//Store the current state of the given block.
	current_x = ( unsigned char )block->x;

	//Move the block left.
	block->x -= 1;

	//Perform a collision check.
	if( collision_check( block, board, shape_set ) )
	{
		//If it fails, revert the block to its previous state and return.
		block->x = current_x;
		return FALSE;
	}

	return TRUE;
}

unsigned char move_right( block_struct *const block, const board_struct *const board, const shape_set_struct *const shape_set )
{
	const shape_rotation_struct *rotation;
	unsigned char current_x;

	//Store a pointer to the current block's shape/rotation.
	rotation = shape_set->shape[ block->shape_type ].rotation + block->rotation;

	//Store the current rotation state of the given block.
	current_x = ( unsigned char )block->x;

	//Move the block right.
	block->x += 1;

	//Perform a collision check.
	if( collision_check( block, board, shape_set ) )
	{
		//If it fails, revert the block to its previous state and return.
		block->x = current_x;
		return FALSE;
	}

	return TRUE;
}

void move_to_bottom( block_struct *const block, const board_struct *const board, const shape_set_struct *const shape_set )
{
	while( move_down( block, board, shape_set ) );
}

//Removes a line of the given index (line) from the Tetris grid by moving the above lines down over it.
static void remove_line( board_struct *const board, const unsigned short line )
{
	short y;

	for( y = line; y >= board->highest_line + 1; --y )
		memcpy( board->unit + y * board->width, board->unit + ( y - 1 ) * board->width, sizeof( board_unit_struct ) * board->width );
	memset( board->unit + board->highest_line * board->width, 0, sizeof( board_unit_struct ) * board->width );

	board->highest_line += 1;
}

//Check the Tetris grid for any completed lines. Returns the amount of completed lines that were removed.
unsigned short check_lines( board_struct *const board )
{
	unsigned short y, x, line_count;

	line_count = 0;

	for( y = board->highest_line; y < board->height; ++y )
	{
		for( x = 0; x < board->width; ++x )
			if( board->unit[ y * board->width + x ].state == 0 )
				break;

		if( x == board->width )
		{
			remove_line( board, y );
			++line_count;
		}
	}

	return line_count;
}

//Count the amount of enclosed holes that are present in the board.
//A hole is defined as a unit of the board between the highest line and
//the bottom of the board that is empty.
unsigned short count_holes( const board_struct *const board )
{
	unsigned short holes;
	short x, y;

	holes = 0;

//If a column of empty spaces has a block above it, it's considered a hole.
//If a column of holes
//If a group of holes is completed enclosed on its right, left and top sides, it's worst.
	for( y = board->highest_line + 1; y < board->height; ++y )
		for( x = 0; x < board->width; ++x )
			if( board->unit[ y * board->width + x ].state == 0 )
				if( board->unit[ ( y - 1 ) * board->width + x ].state == 1 )
					++holes;

	return holes;
}

void set_shape( const block_struct *const block, board_struct *const board, const shape_set_struct *const shape_set )
{
	const shape_rotation_struct *rotation;
	unsigned char y, x;

	rotation = shape_set->shape[ block->shape_type ].rotation + block->rotation;
	//Make sure that it is within the range of the board.

	//Scan the rotation for the first solid unit.
	for( y = 0; y < rotation->height; ++y )
	{
		for( x = 0; x < rotation->width; ++x )
			if( rotation->array[ y * rotation->width + x ] == 1 )
				break;
		if( x < rotation->width )
			break;
	}

	//If the block is empty, return without doing anything.
	if( y == rotation->height )
		return;

	//Update the index of the highest line on the board.
	if( block->y + y < board->highest_line )
		board->highest_line = block->y + y;

	//Continue iterating through the rest of the units of the rotation.
	for( ; y < rotation->height; ++y )
		for( x = 0; x < rotation->width; ++x )
		{
			//Make sure that the current unit of the block is not transparent.
			if( rotation->array[ y * rotation->width + x ] == 0 )
				continue;

			board->unit[ ( block->y + y ) * board->width + block->x + x ].state = 1;
			board->unit[ ( block->y + y ) * board->width + block->x + x ].shape_type = block->shape_type;
		}
}

//Should be called each time a block has hit another object and needs to be set.
//Returns false if the failure/quit condition is fullfilled.
static unsigned char block_has_hit_bottom( game_struct *const game )
{
	//Set the block as part of the board.
	set_shape( &game->current_block, &game->board, &game->shape_set );

	//Remove any lines that are now complete.
	check_lines( &game->board );

	//Set the next block to be the current block.
	memcpy( &game->current_block, &game->next_block, sizeof( block_struct ) );

	//For computer-controlled games, tell the AI that a new block was created.
	if( !game->playable )
		ai_new_block( game );

	//Create a new block to use as the next block.
	new_block( &game->next_block, &game->board, &game->shape_set );

	//If the new current block is already colliding with another block, the game
	//has been lost.
	if( collision_check( &game->current_block, &game->board, &game->shape_set ) )
		return FALSE;

	return TRUE;
}

static unsigned char setup_system_keys( key_struct *const key )
{
	//Assign the default bindings.
	if( !input_set_key( key + SYSTEM_KEY_PAUSE, 1, SDLK_PAUSE, KMOD_NONE )
	 || !input_set_key( key + SYSTEM_KEY_FULLSCREEN, 2, SDLK_RETURN, KMOD_LALT, SDLK_RETURN, KMOD_RALT )
	 || !input_set_key( key + SYSTEM_KEY_QUIT, 1, SDLK_ESCAPE, KMOD_NONE ) )
	{
		input_free_keys( key, SYSTEM_KEY_COUNT );
		return FALSE;
	}

	//Register the keys.
	if( !input_register_keys( key, SYSTEM_KEY_COUNT ) )
	{
		input_free_keys( key, SYSTEM_KEY_COUNT );
		return FALSE;
	}

	return TRUE;
}

static void free_system_keys( key_struct *const key )
{
	input_unregister_keys( key, SYSTEM_KEY_COUNT );
	input_free_keys( key, SYSTEM_KEY_COUNT );
}

static void set_default_game_preferences_human( game_preferences_struct *const game_preferences )
{
	memset( game_preferences, 0, sizeof( game_preferences_struct ) );

	//Assign the default bindings.
	input_set_key( game_preferences->key + GAME_KEY_LEFT, 1, SDLK_LEFT, KMOD_NONE );
	input_set_key( game_preferences->key + GAME_KEY_RIGHT, 1, SDLK_RIGHT, KMOD_NONE );
	input_set_key( game_preferences->key + GAME_KEY_ROTATE_CLOCKWISE, 1, SDLK_UP, KMOD_NONE );
	input_set_key( game_preferences->key + GAME_KEY_ROTATE_COUNTERCLOCKWISE, 1, SDLK_LCTRL, KMOD_NONE );
	input_set_key( game_preferences->key + GAME_KEY_TURBO, 1, SDLK_DOWN, KMOD_NONE );
	input_set_key( game_preferences->key + GAME_KEY_DROP, 1, SDLK_SPACE, KMOD_NONE );

	//Human playable.
	game_preferences->playable = 1;
}

static void set_default_game_preferences_cpu( game_preferences_struct *const game_preferences )
{
	memset( game_preferences, 0, sizeof( game_preferences_struct ) );

	//Assign the default bindings.
	input_set_key( game_preferences->key + GAME_KEY_LEFT, 0 );
	input_set_key( game_preferences->key + GAME_KEY_RIGHT, 0 );
	input_set_key( game_preferences->key + GAME_KEY_ROTATE_CLOCKWISE, 0 );
	input_set_key( game_preferences->key + GAME_KEY_ROTATE_COUNTERCLOCKWISE, 0 );
	input_set_key( game_preferences->key + GAME_KEY_TURBO, 0 );
	input_set_key( game_preferences->key + GAME_KEY_DROP, 0 );

	//Computer-controlled.
	game_preferences->playable = 0;
}

static void process_game( game_struct *const game, const double dt, system_state_struct *const system_state )
{
	game->timer += dt;

	while( game->timer >= ( game->turbo ? game->turbo_rate : game->fall_rate ) )
	{
		game->timer -= ( game->turbo ? game->turbo_rate : game->fall_rate );

		if( !move_down( &game->current_block, &game->board, &game->shape_set ) )
			if( !block_has_hit_bottom( game ) )
				system_state->quit = 1;
	}
}

static void process_events( system_state_struct *const system_state )
{
	SDL_Event event;

	//Check for SDL events.
	while( SDL_PollEvent( &event ) != 0 )
	{
		switch( event.type )
		{
			case SDL_ACTIVEEVENT:
				system_state->active = SDL_GetAppState();
				system_state->active = ( system_state->active & SDL_APPINPUTFOCUS ) && ( system_state->active & SDL_APPACTIVE );
				break;
			case SDL_KEYDOWN:
				audio_play_sample( 0, SDL_MIX_MAXVOLUME, 127 );
			case SDL_KEYUP:
				input_process_key_event( &event.key );
				break;
			case SDL_QUIT:
				system_state->quit = 1;
				break;
			default:
				break;
		}
	}

				/*switch( event.key.keysym.sym )
				{
					case SDLK_ESCAPE:
						quit = TRUE;
						break;
					case SDLK_PAUSE:
						paused = !paused;
						break;
					case SDLK_SPACE:
						space_pressed = 1;
						break;
					case SDLK_LEFT:
						left_pressed = 1;
						break;
					case SDLK_RIGHT:
						right_pressed = 1;
						break;
					case SDLK_UP:
						up_pressed = 1;
						break;
					case SDLK_DOWN:
						down_pressed = 1;
						if( game.turbo_rate < game.fall_rate )
						{
							if( game.timer > game.turbo_rate )
								game.timer = game.turbo_rate;
							game.turbo = 1;
						}
						break;
					case SDLK_RETURN:
						if( event.key.keysym.mod & KMOD_ALT )
						{
							video_preferences->fullscreen = !video_preferences->fullscreen;
							if( !video_toggle_fullscreen( video_info ) )
							{
								video_unload_texture( block );
								video_unload_texture( border );
								video_unload_texture( background );

								set_display_mode( video_preferences, video_info );

								video_reload_texture( block );
								video_reload_texture( border );
								video_reload_texture( background );
							}
						}
						break;
					default:
						break;
				}*/

				/*switch( event.key.keysym.sym )
				{
					case SDLK_DOWN:
						game.turbo = 0;
						break;
					default:
						break;
				}*/
}

static void process_system_input( key_struct *const key, system_state_struct *const system_state, video_struct *const video )
{
	//Process system keys.
	if( key[ SYSTEM_KEY_PAUSE ].state.pressed )
	{
		system_state->paused = !system_state->paused;
		key[ SYSTEM_KEY_PAUSE ].state.pressed = FALSE;
	}
	if( key[ SYSTEM_KEY_FULLSCREEN ].state.pressed )
	{
		video_toggle_fullscreen( video );
		key[ SYSTEM_KEY_FULLSCREEN ].state.pressed = FALSE;
	}
	if( key[ SYSTEM_KEY_QUIT ].state.pressed )
	{
		system_state->quit = 1;
		key[ SYSTEM_KEY_QUIT ].state.pressed = FALSE;
	}
}

static void process_game_input( game_struct *const game, system_state_struct *const system_state )
{
	if( !game->playable )
		ai_process( game );

	//Process game keys.
	if( game->key[ GAME_KEY_LEFT ].state.pressed )
	{
		move_left( &game->current_block, &game->board, &game->shape_set );
		game->key[ GAME_KEY_LEFT ].state.pressed = FALSE;
	}
	if( game->key[ GAME_KEY_RIGHT ].state.pressed )
	{
		move_right( &game->current_block, &game->board, &game->shape_set );
		game->key[ GAME_KEY_RIGHT ].state.pressed = FALSE;
	}
	if( game->key[ GAME_KEY_ROTATE_CLOCKWISE ].state.pressed )
	{
		rotate_clockwise( &game->current_block, &game->board, &game->shape_set );
		game->key[ GAME_KEY_ROTATE_CLOCKWISE ].state.pressed = FALSE;
	}
	if( game->key[ GAME_KEY_ROTATE_COUNTERCLOCKWISE ].state.pressed )
	{
		rotate_counterclockwise( &game->current_block, &game->board, &game->shape_set );
		game->key[ GAME_KEY_ROTATE_COUNTERCLOCKWISE ].state.pressed = FALSE;
	}
	if( game->key[ GAME_KEY_TURBO ].state.pressed )
	{
		if( game->turbo_rate < game->fall_rate )
		{
			if( game->timer > game->turbo_rate )
				game->timer = game->turbo_rate;
			game->turbo = 1;
		}
		game->key[ GAME_KEY_TURBO ].state.pressed =FALSE;
	}
	if( game->key[ GAME_KEY_TURBO ].state.released )
	{
		game->turbo = 0;
		game->key[ GAME_KEY_TURBO ].state.released = FALSE;
	}
	if( game->key[ GAME_KEY_DROP ].state.pressed )
	{
		move_to_bottom( &game->current_block, &game->board, &game->shape_set );
		if( !block_has_hit_bottom( game ) )
			system_state->quit = 1;
		game->key[ GAME_KEY_DROP ].state.pressed = FALSE;
	}
}

static void draw_scene( const game_struct *const game, const unsigned char game_count, const video_struct *const video )
{
	colour_struct *current_colour;
	unsigned char i;
	unsigned short x, y, offset_x, offset_y;

	//Clear the screen.
	video_clear_screen();

	for( i = 0; i < game_count; ++i )
	{
		//Determine the horizontal and vertical offests for the current game board.
		offset_x = i * GAME_WIDTH;
		offset_y = 0;

		//Draw the background picture.
		video_blit_texture( video->texture[ TEXTURE_BACKGROUND ], offset_x + BORDER_WIDTH, offset_y + BORDER_HEIGHT, 1.0, 1.0, 1.0, 1.0 );

		//Draw the fixed Tetris blocks.
		{
			board_unit_struct *current_unit;

			for( y = 0, current_unit = game[ i ].board.unit; y < game[ i ].board.height; ++y )
				for( x = 0; x < game[ i ].board.width; ++x, ++current_unit )
					if( current_unit->state == 1 )
					{
						current_colour = &game[ i ].shape_set.shape[ current_unit->shape_type ].colour;
						video_blit_texture( video->texture[ TEXTURE_BLOCK ], offset_x + ( x + 1 ) * 16, offset_y + ( y + 1 ) * 16,
							current_colour->r, current_colour->g, current_colour->b, 1.0 );
					}
		}

		//Draw the current block and the destination block.
		{
			block_struct destination_block;
			shape_rotation_struct *rotation;
			unsigned char *current_unit;

			//Generate the destination block to show the player where the block will land.
			memcpy( &destination_block, &game[ i ].current_block, sizeof( block_struct ) );
			move_to_bottom( &destination_block, &game[ i ].board, &game[ i ].shape_set );

			rotation = game[ i ].shape_set.shape[ destination_block.shape_type ].rotation + destination_block.rotation;
			current_colour = &game[ i ].shape_set.shape[ destination_block.shape_type ].colour;

			for( y = 0, current_unit = rotation->array; y < rotation->height; ++y )
				for( x = 0; x < rotation->width; ++x, ++current_unit )
					if( *current_unit == 1 )
					{
						video_blit_texture( video->texture[ TEXTURE_BLOCK ], offset_x + ( destination_block.x + x + 1 ) * 16, offset_y + ( game[ i ].current_block.y + y + 1 ) * 16,
							current_colour->r, current_colour->g, current_colour->b, 1.0 );
						video_blit_texture( video->texture[ TEXTURE_BLOCK ], offset_x + ( destination_block.x + x + 1 ) * 16, offset_y + ( destination_block.y + y + 1 ) * 16,
							current_colour->r, current_colour->g, current_colour->b, 0.25 );
					}
		}

		//Draw the vertical borders.
		video_blit_texture( video->texture[ TEXTURE_LBORDER ], offset_x, offset_y, 1.0, 1.0, 1.0, 1.0 );
		video_blit_texture( video->texture[ TEXTURE_RBORDER ], offset_x + BOARD_WIDTH + BORDER_WIDTH, offset_y, 1.0, 1.0, 1.0, 1.0 );

		//Draw the horizontal borders.
		video_blit_texture( video->texture[ TEXTURE_HBORDER ], offset_x + 16, offset_y, 1.0, 1.0, 1.0, 1.0 );
		video_blit_texture( video->texture[ TEXTURE_HBORDER ], offset_x + 16, offset_y + BOARD_HEIGHT + BORDER_HEIGHT, 1.0, 1.0, 1.0, 1.0 );
	}

	video_refresh_screen( &video->info );
}

static void run_games( game_struct *const game, const unsigned char game_count, key_struct *const system_key, video_struct *const video )
{
	system_state_struct system_state;
	Uint32 t1, t2;
	double dt;
	unsigned char i;

	//Initialise game system variables.
	system_state.quit = 0;
	system_state.active = 1;
	system_state.paused = 0;

	//Store the current amount of time.
	t1 = SDL_GetTicks();

	while( !system_state.quit )
	{
		//Wait until a measurable amount of time has elapsed.
		while( ( t2 = SDL_GetTicks() ) == t1 )
			SDL_Delay( 1 );

		//Store the difference in time since the last frame.
		dt = ( double )( t2 - t1 ) / 1000.0;
		t1 = t2;

		process_events( &system_state );

		//If the game is active and not paused, process it.
		if( system_state.active && !system_state.paused )
			for( i = 0; i < game_count; ++i )
			{
				process_game( game + i, dt, &system_state );
				process_game_input( game + i, &system_state );
			}
		else //Otherwise release some CPU time to the host OS.
			SDL_Delay( 1 );

		process_system_input( system_key, &system_state, video );
		draw_scene( game, game_count, video );
	}
}

int do_game()
{
	video_struct video;
	audio_info_struct audio;
	key_struct system_key[ SYSTEM_KEY_COUNT ];
	game_preferences_struct game_preferences;
	game_struct game[ 2 ];

	//Load settings.
	if( !load_settings( &video ) )
		return EXIT_FAILURE;

	//Set up the video things.
	if( !setup_video( &video ) )
		return EXIT_FAILURE;
	
	//Set up audio.
	audio.stereo = TRUE;
	audio.reverse_stereo = FALSE;
	audio.frequency = 96000;
	audio.bps = 16;
	audio.samples = 1024;
	if( !audio_init( &audio, 0 ) )
	{
		free_video( &video );
		return EXIT_FAILURE;
	}
	if( !audio_load_sample( "sound.wav", 0 ) )
	{
		audio_deinit();
		free_video( &video );
		return EXIT_FAILURE;
	}

	//Set up input.
	if( !input_init() )
	{
		audio_deinit();
		free_video( &video );
		return EXIT_FAILURE;
	}

	//Bind the default keys.
	if( !setup_system_keys( system_key ) )
	{
		audio_deinit();
		free_video( &video );
		return EXIT_FAILURE;
	}

	set_default_game_preferences_human( &game_preferences );

	//Create the first game (player-controlled).
	if( !new_game( game, &game_preferences, SDL_GetTicks() ) )
	{
		input_deinit();
		audio_deinit();
		free_video( &video );
		return EXIT_FAILURE;
	}
	
	input_free_keys( game_preferences.key, GAME_KEY_COUNT );
	set_default_game_preferences_human( &game_preferences );

	//Create the second game (computer-controlled).
	if( !new_game( game + 1, &game_preferences, SDL_GetTicks() ) )
	{
		free_game( game );
		input_deinit();
		audio_deinit();
		free_video( &video );
		return EXIT_FAILURE;
	}
	
	input_free_keys( game_preferences.key, GAME_KEY_COUNT );

	//Run the game.
	run_games( game, 2, system_key, &video );

	//Free resources.
    free_game( game );
	free_game( game + 1 );
	free_system_keys( system_key );
    input_deinit();
	audio_deinit();
    free_video( &video );

	return EXIT_SUCCESS;
}
