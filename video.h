#ifndef _VIDEO_H_
#define _VIDEO_H_

#include "SDL.h"

typedef enum
{
	VIDEO_SDL,
	VIDEO_OPENGL
} video_renderer_enum;

typedef struct
{
	video_renderer_enum renderer;
	unsigned short windowed_width;
	unsigned short windowed_height;
	unsigned short fullscreen_width;
	unsigned short fullscreen_height;
	unsigned short bpp;
	unsigned fullscreen : 1;
	unsigned vsync : 1;
	unsigned alpha_blending : 1;
	unsigned subpixel : 1;
} video_info_struct;

//Functions
unsigned char video_init( const video_info_struct *const video_info, const unsigned char texture_count );
__inline__ void video_deinit();
__inline__ void video_toggle_fullscreen();

__inline__ unsigned char video_load_texture( const char *const file, const unsigned long id );
__inline__ unsigned char video_reload_texture( const unsigned long index );
__inline__ void video_free_texture( const unsigned long index );
__inline__ unsigned char video_reload_all_textures();
__inline__ void video_free_all_textures();

__inline__ void video_blit_texture( const unsigned long index, const long x, const long y );
__inline__ void video_refresh();
__inline__ void video_set_clear_colour( unsigned char r, unsigned char g, unsigned char b );
__inline__ void video_clear();

#endif
