#include <stdlib.h>
#include <SDL.h>
#include "main.h"
#include "game.h"

int main( int argc, char **argv )
{
	int ret_val;
	
	//Initialise SDL.
	if( SDL_Init( 0 ) != 0 )
	{
		fprintf( stderr, "Failed to initialise SDL: %s\n", SDL_GetError() );
		return EXIT_FAILURE;
	}
	atexit( SDL_Quit );
	
	//Execute the game.
	ret_val = do_game();
	
	//De-initialise SDL.
	SDL_Quit();
	
	return ret_val;
}
