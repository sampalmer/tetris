#include <math.h>
#include <SDL.h>
#include "main.h"
#include "audio.h"

#define MAX_CHANNELS 16
#define MAX_SAMPLES 255

typedef struct
{
	char *filename;
	unsigned char *data;
	unsigned long len;
	unsigned char loaded;
	unsigned char in_use;
	unsigned short playing_count; //Stores the amount of channels currently playing the sample.
} audio_sample_struct;

typedef struct
{
	unsigned char *data;
	unsigned long len;
	unsigned long pos;
	unsigned char in_use;
	unsigned char playing;
	unsigned short *playing_count; //A pointer to the sample's playing_count.
	unsigned char volume; //0 - SDL_MIX_MAXVOLUME
	unsigned char pan; //0 - left, 254 - right, 127 - centre
} audio_channel_struct;

static struct
{
	unsigned char initialised;
	
	//Audio information
	audio_info_struct info;
	SDL_AudioSpec spec;
	
	//Audio channels.
	audio_channel_struct channel[ MAX_CHANNELS ];
	audio_channel_struct *next_unused_channel;
	
	//Audio samples.
	audio_sample_struct *sample;
	unsigned short sample_count;
} audio = { 0 };

static void audio_callback( void *userdata, Uint8 *stream, int len )
{
	audio_channel_struct *channel, *last_channel;
	unsigned long mix_len;

	channel = audio.channel;
	last_channel = channel + MAX_CHANNELS;
	
	//Iterate through each audio channel that is occupied and playing.
	for( ; channel < last_channel; ++channel )
		if( channel->in_use && channel->playing )
		{
			//Determine if the length of the sound buffer is at least as long as the
			//remaining length of the sound channel.
			if( ( unsigned long )len >= channel->len - channel->pos )
			{
				//If so, stop the sound channel so that it will no longer play.
				channel->in_use = FALSE;
				*channel->playing_count -= 1;
				if( audio.next_unused_channel == NULL || audio.next_unused_channel > channel )
					audio.next_unused_channel = channel;
				mix_len = channel->len - channel->pos;
			}
			else
			{
				mix_len = len;
			}
			
			//Mix the current channel into the stream.
			SDL_MixAudio( stream, channel->data + channel->pos, mix_len, channel->volume );

			channel->pos += mix_len;
		}
}

//Initialises the audio subsystem for use, and pre-allocates memory for the
//given number of samples.
unsigned char audio_init( const audio_info_struct *const info, const unsigned short sample_count )
{
	SDL_AudioSpec audiospec;
	
	//Validate.
	if( audio.initialised )
		return FALSE;
	if( info->bps != 8 && info->bps != 16 )
		return FALSE;
	if( sample_count > MAX_SAMPLES )
		return FALSE;
	
	//Initialise SDL's audio subsystem.
	if( SDL_InitSubSystem( SDL_INIT_AUDIO ) == -1 )
		return FALSE;
	
	//Fill in the desired audio specifications.
	audiospec.freq = info->frequency;
	audiospec.format = info->bps == 8 ? AUDIO_S8 : AUDIO_S16SYS;
	audiospec.channels = info->stereo ? 2 : 1;
	audiospec.samples = info->samples;
	audiospec.callback = audio_callback;
	audiospec.userdata = NULL;
	
	//Open the audio device.
	if( SDL_OpenAudio( &audiospec, &audio.spec ) == -1 )
	{
		SDL_QuitSubSystem( SDL_INIT_AUDIO );
		return FALSE;
	}

	//Fill in the audio info structure with the obtained audio information.
	audio.info.stereo = audio.spec.channels == 2;
	audio.info.frequency = audio.spec.freq;
	if( audio.spec.format == AUDIO_U8 || audio.spec.format == AUDIO_S8 )
		audio.info.bps = 8;
	else
		audio.info.bps = 16;
	audio.info.samples = audio.spec.samples;

	//Clear all audio channels.
	memset( audio.channel, 0, sizeof( audio_channel_struct ) * MAX_CHANNELS );
	audio.next_unused_channel = audio.channel;
	
	//Pre-allocate memory for the requested number of samples.
	if( sample_count != 0 )
	{
		audio.sample = ( audio_sample_struct * )malloc( sizeof( audio_sample_struct ) * sample_count );
		if( audio.sample == NULL )
		{
			//If this fails, don't worry about allocating memory for samples yet.
			audio.sample_count = 0;
		}
		else
		{
			memset( audio.sample, 0, sizeof( audio_sample_struct ) * sample_count );
			audio.sample_count = sample_count;
		}
	}
	else
	{
		audio.sample = NULL;
		audio.sample_count = 0;
	}

	//Fill in the rest of the info.
	audio.initialised = TRUE;

	//Begin in_use audio (starting off with silence).
	SDL_PauseAudio( 0 );

	return TRUE;
}

//Deinitialises the audio subsystem.
void audio_deinit()
{
	//Validate.
	if( !audio.initialised )
		return;

	audio_free_all_samples( FALSE );
	SDL_CloseAudio();
	SDL_QuitSubSystem( SDL_INIT_AUDIO );
	audio.initialised = FALSE;
}

//Frees an audio sample without performing any checking or locking.
static void _audio_free_sample( audio_sample_struct *const sample )
{
	free( sample->data );
	
	if( sample->filename != NULL )
		free( sample->filename );

	sample->in_use = FALSE;
}

//Resizes the internal array of samples.
static unsigned char audio_resize_sample_array( const unsigned short new_sample_count )
{
	audio_sample_struct *last_sample;
	
	//If the array is being decreased in size, free any samples that are in use that will be removed.
	if( new_sample_count < audio.sample_count )
	{
		unsigned char sample_still_playing = TRUE;
		audio_sample_struct *sample;
		
		sample = audio.sample + new_sample_count;
		last_sample = audio.sample + audio.sample_count;
		
		//Iterate through the affected samples.
		SDL_LockAudio();
		for( ; sample < last_sample; ++sample )
			if( sample->in_use )
			{
				//Make sure that any unfreed samples are not currently playing.
				if( sample->playing_count == 0 )
					_audio_free_sample( sample );
				else
					sample_still_playing = TRUE;;
			}
		SDL_UnlockAudio();

		//If one of the affected samples is still playing, return and don't resize the array.
		if( sample_still_playing )
			return FALSE;
	}
	
	//Resize the array.
	last_sample = audio.sample;
	audio.sample = ( audio_sample_struct * )realloc( last_sample, sizeof( audio_sample_struct ) * new_sample_count );
	if( audio.sample == NULL )
	{
		//If the required amount of memory can't be allocated, return.
		audio.sample = last_sample;
		return FALSE;
	}
	
	//If the array is being increased in size, zero any new samples.
	if( new_sample_count > audio.sample_count )
		memset( audio.sample + audio.sample_count, 0, sizeof( audio_sample_struct ) * ( new_sample_count - audio.sample_count ) );
	
	//Store the new sample count.
	audio.sample_count = new_sample_count;

	return TRUE;
}

//Loads an audio sample from the given file, and assigns it the supplied id number.
unsigned char audio_load_sample( const char *file, const unsigned short id )
{
	SDL_AudioSpec spec;
	SDL_AudioCVT cvt;
	Uint8 *data;
	Uint32 len;
	audio_sample_struct *sample;
	unsigned short old_sample_count = audio.sample_count;
	
	//Validate the supplied information.
	if( file == NULL )
		return FALSE;
	if( id > MAX_SAMPLES - 1 )
		return FALSE;
	if( id < audio.sample_count )
		if( audio.sample[ id ].in_use )
			return FALSE;
	
	//Attempt to load the file.
	if( SDL_LoadWAV( file, &spec, &data, &len ) == NULL )
		return FALSE;
//TODO: SDL_BuildAudioCVT returns 1 if successful, but also returns 0 if no conversion is necessary. Handle this case without wasting processing time.
//TODO2: It turns out that SDL's audio conversion is crap. Consider SDL_sound from icculus.org.
	//Fill in the SDL audio conversion structure.
	if( SDL_BuildAudioCVT( &cvt, spec.format, spec.channels, spec.freq, audio.spec.format, audio.spec.channels, audio.spec.freq ) == -1 )
	{
		SDL_FreeWAV( data );
		return FALSE;
	}
	cvt.buf = ( Uint8 * )malloc( sizeof( Uint8 ) * len * cvt.len_mult );
	if( cvt.buf == NULL )
	{
		SDL_FreeWAV( data );
		return FALSE;
	}
	cvt.len = len;
	memcpy( cvt.buf, data, sizeof( Uint8 ) * len );
	SDL_FreeWAV( data );

	//Convert the audio into the current hardware audio format.
	if( SDL_ConvertAudio( &cvt ) != 0 )
	{
		free( cvt.buf );
		return FALSE;
	}

	//Check memory is already allocated for the requested ID.
	if( id >= audio.sample_count )
	{
		//If not, reallocate the internal array of samples to make room for the new one.
		if( !audio_resize_sample_array( id + 1 ) )
		{
			//If this fails, return.
			free( cvt.buf );
			return FALSE;
		}
	}

	//Fill in the sample information.
	sample = audio.sample + id;
	sample->filename = _strdup( file );
	if( sample->filename == NULL )
	{
		//If there isn't enough memory to store the file's file name, return.
		free( cvt.buf );
		if( audio.sample_count != old_sample_count )
			audio_resize_sample_array( old_sample_count );
		return FALSE;
	}
	sample->data = cvt.buf;
	sample->len = ( unsigned long )ceil( ( double )cvt.len * cvt.len_ratio );
	sample->loaded = TRUE;
	sample->in_use = TRUE;
	sample->playing_count = 0;
	
	return TRUE;
}

//Frees memory associated with a sample that was previously loaded.
//If 'temporary' is non-zero, the memory associated with the sample will remain allocated
//to allow a subsequent sample load to re-use the given id number.
void audio_free_sample( const unsigned short id, const unsigned char temporary )
{
	audio_sample_struct *sample;

	sample = audio.sample + id;

	//Validate the supplied information.
	if( id > MAX_SAMPLES - 1 )
		return;
	if( !sample->in_use )
		return;
	if( sample->playing_count != 0 )
		return;
	
	//Free all memory associated with the sample.
	SDL_LockAudio();
	_audio_free_sample( sample );
	SDL_UnlockAudio();

	//If temporary is zero (or FALSE), resize the array of samples if possible.
	if( !temporary )
	{
		sample = audio.sample + audio.sample_count - 1;
		
		//Iterate backwards through each sample and break at the first one in use.
		for( ; sample >= audio.sample; --sample )
			if( sample->in_use )
				break;
		
		//Resize the array.
		audio_resize_sample_array( sample - audio.sample + 1 );
	}
}

//Plays a sample.
unsigned char audio_play_sample( const unsigned short id, const unsigned char volume, const unsigned char pan )
{
	audio_channel_struct *channel;
	audio_sample_struct *sample;

	sample = audio.sample + id;
	
	//Validate the supplied information.
	if( id > MAX_SAMPLES - 1 )
		return FALSE;
	if( !( sample->in_use && sample->loaded ) )
		return FALSE;

/*TODO:
  If a sample is 'in use' (occupied) but not loaded,
  load the sample here.
*/
	//Get a pointer to the next available channel, if one is available.
	SDL_LockAudio();
	channel = audio.next_unused_channel;
	if( channel == NULL )
	{
		SDL_UnlockAudio();
		return FALSE;
	}
	
	//Assign the sample to the channel and start playing.
	channel->data = sample->data;
	channel->len = sample->len;
	channel->pos = 0;
	channel->volume = volume;
	channel->pan = pan;
	channel->playing_count = &sample->playing_count;
	channel->in_use = TRUE;
	channel->playing = TRUE;
	sample->playing_count += 1;

	//Update the 'next_unused_channel' pointer.
	channel = audio.channel + MAX_CHANNELS;
	for( ; audio.next_unused_channel < channel; ++audio.next_unused_channel )
		if( !audio.next_unused_channel->in_use )
			break;
	
	//If a free channel could not be found, assign the pointer to NULL to indicate this.
	if( audio.next_unused_channel == channel )
		audio.next_unused_channel = NULL;
	SDL_UnlockAudio();

	return TRUE;
}

//Clears all audio channels so that no sound is playing.
void audio_stop_all_channels()
{
	audio_channel_struct *channel, *last_channel;
	
	SDL_LockAudio();
	channel = audio.channel;
	last_channel = channel + MAX_CHANNELS;

	//Iterate through each channel and stop it.
	for( ; channel < last_channel; ++channel )
		if( channel->in_use )
		{
			channel->in_use = FALSE;
			*channel->playing_count -= 1;
		}
	
	//Update the 'next_unused_channel' pointer.
	audio.next_unused_channel = audio.channel;
	SDL_UnlockAudio();
}

//Sets all audio channels that are in use to not be playing.
void audio_pause_all_channels()
{
	audio_channel_struct *channel, *last_channel;
	
	SDL_LockAudio();
	channel = audio.channel;
	last_channel = channel + MAX_CHANNELS;

	//Iterate through each channel and pause it it.
	for( ; channel < last_channel; ++channel )
		if( channel->in_use )
			channel->playing = FALSE;
	SDL_UnlockAudio();
}

//Sets all audio channels that are in use to be playing.
void audio_unpause_all_channels()
{
	audio_channel_struct *channel, *last_channel;

	SDL_LockAudio();
	channel = audio.channel;
	last_channel = channel + MAX_CHANNELS;

	//Iterate through each channel and pause it it.
	for( ; channel < last_channel; ++channel )
		if( channel->in_use )
			channel->playing = TRUE;
	SDL_UnlockAudio();
}

//Frees all loaded audio samples. 
//If 'temporary' is non-zero, the memory associated with the samples will remain allocated
//to allow a subsequent sample load to re-use the memory immediately afterwards.
void audio_free_all_samples( unsigned char temporary )
{
	audio_sample_struct *sample, *last_sample;
	
	audio_stop_all_channels();
	
	sample = audio.sample;
	last_sample = sample + audio.sample_count;
	
	//Iterate through each sample and free it if it is in use.
	SDL_LockAudio();
	for( ; sample < last_sample; ++sample )
		if( sample->in_use )
			_audio_free_sample( sample );
	
	if( !temporary )
	{
		//Free the array.
		free( audio.sample );
		audio.sample_count = 0;
	}
	SDL_UnlockAudio();
}
