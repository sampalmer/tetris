#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include "main.h"
#include "config.h"

#define LINE_BUFFER_SIZE_INCREMENT 80

//Reads in a line from the given file and returns a
//buffer allocated using malloc() containing the line.
static char *config_read_line( FILE *fp )
{
	char *buffer, *buffer_end, *p;
	int buffer_size, c;
	
	//Initialise the buffer.
	buffer_size = LINE_BUFFER_SIZE_INCREMENT;
	buffer = ( char * )malloc( sizeof( char ) * buffer_size );
	if( buffer == NULL )
		return NULL;
	
	//Set the current position in the buffer, and get a pointer to the buffer end.
	p = buffer;
	buffer_end = buffer + buffer_size - 1;
	
	//Iterate through each character in the file.
	do
	{
		c = fgetc( fp );
		
		//Determine what to do depending on the character that was read in.
		switch( c )
		{
			case EOF:
				if( p == buffer )
				{
					//If no characters were read before the end of file, return NULL.
					free( buffer );
					return NULL;
				}
				else
				{
					*buffer_end = '\0';
					return buffer;
				}
			case '\n':
				*buffer_end = '\0';
				return buffer;
			default:
				//Store the character in the buffer.
				*p = ( char )c;
				++p;
				
				//If the current position in the buffer is at the end of the buffer,
				//reallocate to increase its size.
				if( p == buffer_end )
				{
					int p_offset = p - buffer;
					
					//Increase the buffer size.
					buffer_size += LINE_BUFFER_SIZE_INCREMENT;
					p = buffer;
					buffer = ( char * )realloc( buffer, sizeof( char ) * buffer_size );
					if( buffer == NULL )
					{
						free( p );
						return NULL;
					}
					
					//Re-set the buffer_end and p pointers.
					p = buffer + p_offset;
					buffer_end = buffer + buffer_size - 1;
				}
				break;
		}
	} while( 1 );
}

static config_section_struct *config_new_section( config_struct *config, const char *name )
{
	config_section_struct *section;
	
	//Reallocate the array of sections to make room for this one.
	config->section_count += 1;
	section = config->section;
	config->section = ( config_section_struct * )realloc( section, sizeof( config_section_struct ) * config->section_count );
	if( config->section == NULL )
	{
		config->section_count -= 1;
		config->section = section;
		return NULL;
	}
	
	//Zero the new section.
	section = config->section + config->section_count - 1;
	memset( section, 0, sizeof( config_section_struct ) );

	return section;
}

static void config_add_item( config_section_struct *section, const char *line )
{
	const char *p2, *p;
	config_item_struct *item;
	
	//Get a pointer to the equals sign.
	for( p2 = line; *p2 != '='; ++p2 )
		if( *p2 == '\0' )
			return;
	
	//Ignore a line not containing an equals sign.
	if( *p == '\0' )
		return;

	//Backtrack past any whitespace.
	for( p = p2 - 1; p >= line; --p )
		if( !isspace( *p ) )
			break;
	++p;
	
	//Increase the size of the item array.
	section->item_count += 1;
	item = section->item;
	section->item = ( config_item_struct * )realloc( item, sizeof( config_item_struct ) * section->item_count );
	if( section->item == NULL )
	{
		section->item_count -= 1;
		section->item = item;
		return;
	}
	
	//Copy the item name/key over.
	item = section->item + section->item_count - 1;
	item->key = ( char * )malloc( sizeof( char ) * ( p - line + 1 ) );
	if( item->key == NULL )
		return;
	strncpy( item->key, line, p - line );
	item->key[ p - line ] = '\0';
	
	//Skip past any whitespace after the equals sign.
	for( p = p2 + 1; isspace( *p ) && *p != '\0'; ++p );
	if( *p == '\0' )
	{
		//If no value was specified, ignore this line.
		free( item->key );
		section->item_count -= 1;
		item = section->item;
		section->item = ( config_item_struct * )realloc( item, sizeof( config_item_struct ) * section->item_count );
		if( section->item == NULL )
			section->item = item;
		return;
	}
	
	//Now get a pointer to the end of the line.
	for( p2 = p; *p2 != '\0'; ++p2 );

	//Backtrack past any whitespace.
	for( p2 = p2 - 1; p2 >= p; --p2 )
		if( !isspace( *p2 ) )
			break;
	++p2;

	//Copy the item value over.
	item->value = ( char * )malloc( sizeof( char ) * ( p2 - p + 1 ) );
	if( item->value == NULL )
	{
		free( item->key );
		section->item_count -= 1;
		item = section->item;
		section->item = ( config_item_struct * )realloc( item, sizeof( config_item_struct ) * section->item_count );
		if( section->item == NULL )
			section->item = item;
		return;
	}
	strncpy( item->value, p, p2 - p );
	item->value[ p2 - p ] = '\0';
}

config_struct *config_load( const char *file )
{
	config_struct *config;
	config_section_struct *section;
	char *buffer;
	FILE *fp;
	
	//Allocate memory for the configuration struct.
	config = ( config_struct * )malloc( sizeof( config_struct ) );
	if( config == NULL )
		return NULL;
	memset( config, 0, sizeof( config_struct ) );

	//Open the file.
	fp = fopen( file, "r" );
	if( fp == NULL )
	{
		free( config );
		return NULL;
	}
	
	section = NULL;

	//Read through each line in the file.
	while( ( buffer = config_read_line( fp ) ) != NULL )
	{
		//Skip leading whitespace.
		for( ; isspace( *buffer ) && *buffer != 0; ++buffer );

		switch( *buffer )
		{
			case '[':
			{
				char *p;

				//Get a pointer to the character after the end of the section name.
				for( p = buffer + 1; !( *p == ']' || *p == '\0' ); ++p );
				
				//Null terminate the string at the end of the section name.
				*p = '\0';
				
				//Add the section the config struct.
				section = config_new_section( config, buffer + 1 );
				if( section == NULL )
				{
					free( buffer );
					fclose( fp );
					return config;
				}

				break;
			}
			case ';':
			case '#':
			case '\0':
				break;
			default:
				//Make sure that a section has been specified.
				if( section != NULL )
					config_add_item( section, buffer );

				break;
		}

		free( buffer );
	}

	fclose( fp );
}

void config_free( config_struct *config )
{
	config_section_struct *section, *last_section;
	config_item_struct *item, *last_item;

	section = config->section;
	last_section = section + config->section_count;

	for( ; section < last_section; ++section )
	{
		item = section->item;
		last_item = item + section->item_count;

		for( ; item < last_item; ++item )
		{
			free( item->key );
			free( item->value );
		}

		free( section->item );
	}

	free( config->section );
	free( config );
}
