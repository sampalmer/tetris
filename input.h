#ifndef _INPUT_H_
#define _INPUT_H_

#include <stdarg.h>
#include <SDL.h>

typedef struct
{
	unsigned char pressed, down, released;
} key_state_struct;

typedef struct
{
	SDLKey sym;
	SDLMod mod;
	key_state_struct *state;
} key_binding_struct;

typedef struct
{
	key_binding_struct *binding;
	unsigned char binding_count;

	key_state_struct state;
} key_struct;

typedef struct
{
	key_binding_struct *binding;
	unsigned char binding_count;
} key_handler_struct;

unsigned char input_init();
void input_deinit();
unsigned char input_set_key( key_struct *const key, const unsigned char binding_count, ... );
void input_free_keys( key_struct *key, const unsigned char key_count );
unsigned char input_copy_keys( key_struct *const destination_key, const key_struct *source_key, unsigned char key_count );
unsigned char input_register_keys( const key_struct *key, const unsigned char key_count );
void input_unregister_keys( const key_struct *key, const unsigned char key_count );
void input_process_key_event( SDL_KeyboardEvent *event );

#endif
