#ifndef _AI_H_
#define _AI_H_

#include "game.h"

//AI-specific structs
typedef enum
{
	MOVE_OPERATION_NONE,
	MOVE_OPERATION_LEFT,
	MOVE_OPERATION_RIGHT,
	MOVE_OPERATION_DOWN,
	MOVE_OPERATION_CLOCKWISE,
	MOVE_OPERATION_COUNTERCLOCKWISE
} move_operation_enum;

typedef struct _ai_node_struct
{
	struct _ai_node_struct *parent;
	move_operation_enum move_operation;
	unsigned short g_score;
	block_struct block;
	struct _ai_node_struct *prev, *next;
} ai_node_struct;

typedef struct _ai_struct
{
	ai_node_struct *path;
	ai_node_struct *current_node;
} ai_struct;

typedef struct
{
	char x, y, rotation;
} move_direction_struct;

/*typedef struct
{
	unsigned short x, y;
	unsigned char rotation;
} goal_struct;*/

void ai_process_pathfind_free_node_list( ai_node_struct *node_start );
void ai_new_block( const game_struct *const game );
void ai_process( game_struct *const game );

#endif
