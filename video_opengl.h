#ifndef _VIDEO_OPENGL_H_
#define _VIDEO_OPENGL_H_

unsigned char video_init_opengl( const video_info_struct *const video_info, const unsigned char texture_count );
void video_deinit_opengl();
void video_toggle_fullscreen_opengl();

unsigned char video_load_texture_opengl( const char *const file, const unsigned long index );
unsigned char video_reload_texture_opengl( const unsigned long index );
void video_free_texture_opengl( const unsigned long index );
unsigned char video_reload_all_textures_opengl();
void video_free_all_textures_opengl();

void video_blit_texture_opengl( const unsigned long index, const long x, const long y );
void video_refresh_opengl();
void video_set_clear_colour_opengl( unsigned char r, unsigned char g, unsigned char b );
void video_clear_opengl();

#endif
