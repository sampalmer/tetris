#include <stdlib.h>
#include "main.h"
#include "ai.h"
#include "game.h"

#define PATHFIND_HORIZONTAL_COST 10
#define PATHFIND_ROTATION_COST 10
#define PATHFIND_VERTICAL_COST 10

//#define AI_USE_INSTANT_DROP
#define AI_USE_TURBO

//DEBUG
/*static unsigned char test_nodes( ai_node_struct *list )
{
	ai_node_struct *prev, *node = list;

	//Make sure that the start of the list doesn't link back to a previous node.
	if( node != NULL )
		if( node->prev != NULL )
			return FALSE;

	prev = NULL;

	while( node != NULL )
	{
		//Make sure that the node's previous link matches the one we just came from.
		if( node->prev != prev )
			return FALSE;

		//If a previous link is present, make sure that it links back to the current node.
		if( node->prev != NULL )
			if( node->prev->next != node )
				return FALSE;

		//If a next link is present, make sure that it links back to the current node.
		if( node->next != NULL )
			if( node->next->prev != node )
				return FALSE;

		prev = node;
		node = node->next;
	}

	return TRUE;
}*/

//Fills in the score of the node, which indicates the cost to get to this node from the start.
static void ai_process_pathfind_calculate_scores( ai_node_struct *const node )
{
	//Calculate the g-score of the node (the cost from the list to the node).
	if( node->parent == NULL )
		node->g_score = 0;
	else
		switch( node->move_operation )
		{
			case MOVE_OPERATION_LEFT:
			case MOVE_OPERATION_RIGHT:
				node->g_score = node->parent->g_score + PATHFIND_HORIZONTAL_COST;
				break;
			case MOVE_OPERATION_DOWN:
				node->g_score = node->parent->g_score + PATHFIND_VERTICAL_COST;
				break;
			case MOVE_OPERATION_CLOCKWISE:
			case MOVE_OPERATION_COUNTERCLOCKWISE:
				node->g_score = node->parent->g_score + PATHFIND_ROTATION_COST;
				break;
			case MOVE_OPERATION_NONE:
			default:
				node->g_score = node->parent->g_score;
				break;
		}
}

//Adds the given node to the given linked list.
static void ai_process_pathfind_add_node( ai_node_struct *const node, ai_node_struct **list )
{
	ai_node_struct *link = *list;

	//If the given linked list is empty, set it to the supplied node.
	if( link == NULL )
	{
		*list = node;
		node->prev = NULL;
		node->next = NULL;
		return;
	}

	//Get a pointer to the last node in the linked list.
	for( ; link->next != NULL; link = link->next );

	//Link the node to the end of the linked list.
	link->next = node;
	node->prev = link;
	node->next = NULL;
}

//Creates a new node using the given information, and links it to the supplied list.
static ai_node_struct *ai_process_pathfind_new_node( ai_node_struct **list, ai_node_struct *const parent, const block_struct *block, move_operation_enum move_operation )
{
	ai_node_struct *node;

	//Allocate memory for the new node.
	node = ( ai_node_struct * )malloc( sizeof( ai_node_struct ) );
	if( node == NULL )
		return NULL;

	//Fill in the node's information.
	node->parent = parent;
	node->move_operation = move_operation;
	node->prev = NULL;
	node->next = NULL;
	memcpy( &node->block, block, sizeof( block_struct ) );

	//Calculate the node's scores.
	ai_process_pathfind_calculate_scores( node );

	//Add the node to the given list.
	ai_process_pathfind_add_node( node, list );

	return node;
}

/*static ai_node_struct *ai_process_pathfind_find_lowest_f_score( ai_node_struct *open_list )
{
	unsigned long lowest_f_score;
	ai_node_struct *node;

	//Initialise our 'best' values to those of the first link.
	lowest_f_score = ( unsigned long )-1;
	node = open_list;

	//Iterate through each node in the linked list.
	while( open_list != NULL )
	{
		//If a node is found whose f-score is lower than the currently
		//stored lowest value, store its details as the best node.
		if( open_list->f_score <= lowest_f_score )
		{
			lowest_f_score = open_list->f_score;
			node = open_list;
		}
		open_list = open_list->next;
	}

	return node;
}*/

//Removes the given node from the given list.
static void ai_process_pathfind_remove_node( ai_node_struct *const node, ai_node_struct **const list )
{
	//If the node is linked to a previous node, link the previous node
	//to the next node instead of the current node.
	if( node->prev != NULL )
		node->prev->next = node->next;

	//If the node is linked to a next node, link the next node
	//to the previous node instead of the current node.
	if( node->next != NULL )
		node->next->prev = node->prev;


	//If this node was actually the starting node in the list,
	//change the list to point to the next node.
	if( *list == node )
		*list = node->next;

	node->prev = NULL;
	node->next = NULL;
}

//Frees the given node from memory.
static void ai_process_pathfind_free_node( ai_node_struct *const node )
{
	free( node );
}

//Frees the given list of nodes from memory.
void ai_process_pathfind_free_node_list( ai_node_struct *node_start )
{
	ai_node_struct *temp;

	while( node_start != NULL )
	{
		temp = node_start->next;
		ai_process_pathfind_free_node( node_start );
		node_start = temp;
	}
}

//Given an end-point, this function searches through each node leading up to
//and including the end-point, and removes it from the given list.
static void ai_process_pathfind_remove_all_nodes_to_end_point_from_list( ai_node_struct *end_point, ai_node_struct **const list )
{
	for( ; end_point != NULL; end_point = end_point->parent )
		ai_process_pathfind_remove_node( end_point, list );
}

//Given an end-point, this function searches through each node leading up to
//and including the end-point, and adds it to the given list.
static void ai_process_pathfind_add_all_nodes_to_end_point_to_list( ai_node_struct *end_point, ai_node_struct **const list )
{
	for( ; end_point != NULL; end_point = end_point->parent )
		ai_process_pathfind_add_node( end_point, list );
}

//Moves the given from the source list to the destination list.
static void ai_process_pathfind_move_node( ai_node_struct *const node, ai_node_struct **const source_list, ai_node_struct **const destination_list )
{
	ai_process_pathfind_remove_node( node, source_list );
	ai_process_pathfind_add_node( node, destination_list );
}

//Locates a node whose orientation matches the supplied orientation.
static ai_node_struct *ai_process_pathfind_find_node_by_orientation( ai_node_struct *list, const block_struct *const block )
{
	//Iterate through each node in the list.
	for( ; list != NULL; list = list->next )
	{
		//If the node has details matching the supplied ones, we've found the node.
		if( memcmp( &list->block, block, sizeof( block_struct ) ) == 0 )
			return list;
	}

	return NULL;
}

static void ai_process_pathfind_process_adjacent_node( ai_node_struct *const parent_node, ai_node_struct **open_list, ai_node_struct *closed_list, ai_node_struct *end_list, const block_struct *const block, move_operation_enum move_operation )
{
	ai_node_struct *current_node;

	//See if we can find a node that already exists with these details.
	//Make sure to search both the open and closed lists.
	current_node = ai_process_pathfind_find_node_by_orientation( *open_list, block );
	if( current_node == NULL )
	{
		current_node = ai_process_pathfind_find_node_by_orientation( closed_list, block );
		if( current_node == NULL )
			current_node = ai_process_pathfind_find_node_by_orientation( end_list, block );
	}

	if( current_node == NULL )
	{
		//If another was not present, add a new node at this orientation.
		ai_process_pathfind_new_node( open_list, parent_node, block, move_operation );
	}
	else
	{
		//If another node was present at this orientation, duplicate it with
		//the given parent instead of its previous parent to see if we can reduce
		//its g-cost. If so make the given parent its parent.
		ai_node_struct *temp_node;
		unsigned short new_g_score;

		temp_node = ai_process_pathfind_new_node( open_list, parent_node, block, move_operation );

		if( temp_node != NULL )
		{
			new_g_score = temp_node->g_score;
			ai_process_pathfind_remove_node( temp_node, open_list );
			ai_process_pathfind_free_node( temp_node );
			if( new_g_score < current_node->g_score )
			{
				current_node->parent = parent_node;
				current_node->move_operation = move_operation;
				ai_process_pathfind_calculate_scores( current_node );
			}
		}
	}
}

//The end point nodes' co-ordinates are all one unit beneath the correct one
//to allow for path-finding to operate correctly.
//This function corrects this.
static void ai_process_pathfind_correct_end_points( ai_node_struct *list )
{
	for( ; list != NULL; list = list->next )
		--list->block.y;
}

//Uses path-finding to determine all possible end points for the current block in the
//given game.
static void ai_process_pathfind_find_end_points( ai_node_struct **const open_list, ai_node_struct **const closed_list, ai_node_struct **const end_list, const game_struct *const game )
{
	ai_node_struct *current_node;
	block_struct block;

	while( 1 )
	{
		//Set the current_node node as the node in the open list with the lowest f-score.
		current_node = *open_list;

		//If no more nodes exist in the open list, we have processed all possible moves
		//and can now return.
		if( current_node == NULL )
			break;

		//Add the current_node node to the closed list.
		ai_process_pathfind_move_node( current_node, open_list, closed_list );

		//Create a temporary block.
		memcpy( &block, &current_node->block, sizeof( block_struct ) );

		//Check if we have just added an end-point node to the closed list.
		if( collision_check( &current_node->block, &game->board, &game->shape_set ) )
		{
			//If so, remove it from the closed list and add it to the end point list
			//so we can keep track of the end points.
			ai_process_pathfind_remove_node( current_node, closed_list );
			ai_process_pathfind_add_node( current_node, end_list );
			continue;
		}

		//Check where it would be possible to move from this node, and process accordingly.
		if( rotate_clockwise( &block, &game->board, &game->shape_set ) )
		{
			ai_process_pathfind_process_adjacent_node( current_node, open_list, *closed_list, *end_list, &block, MOVE_OPERATION_CLOCKWISE );
			block.rotation = current_node->block.rotation;
			block.x = current_node->block.x;
			block.y = current_node->block.y;
		}

		if( rotate_counterclockwise( &block, &game->board, &game->shape_set ) )
		{
			ai_process_pathfind_process_adjacent_node( current_node, open_list, *closed_list, *end_list, &block, MOVE_OPERATION_COUNTERCLOCKWISE );
			block.rotation = current_node->block.rotation;
			block.x = current_node->block.x;
			block.y = current_node->block.y;
		}

		if( move_left( &block, &game->board, &game->shape_set ) )
		{
			ai_process_pathfind_process_adjacent_node( current_node, open_list, *closed_list, *end_list, &block, MOVE_OPERATION_LEFT );
			block.x = current_node->block.x;
		}

		if( move_right( &block, &game->board, &game->shape_set ) )
		{
			ai_process_pathfind_process_adjacent_node( current_node, open_list, *closed_list, *end_list, &block, MOVE_OPERATION_RIGHT );
			block.x = current_node->block.x;
		}

		{
			//In the case of moving down, even if the current node can't move down,
			//we'll still consider moving down valid, because that is how the block
			//sets itself as part of the board.
			unsigned char result;

			result = move_down( &block, &game->board, &game->shape_set );
			if( !result )
				++block.y;

			ai_process_pathfind_process_adjacent_node( current_node, open_list, *closed_list, *end_list, &block, MOVE_OPERATION_DOWN );
			block.y = current_node->block.y;
		}
	}

	ai_process_pathfind_correct_end_points( *end_list );
}

//Determines which supplied end point is most advantageous to the given game.
static ai_node_struct *ai_process_pathfind_choose_best_end_point( ai_node_struct *end_list, const game_struct *const game )
{
	//Temporary values.
	board_struct board;
	unsigned short lines_cleared, hole_count;

	//'Best' values.
	ai_node_struct *best_end_point = end_list;
	long lowest_y = -1, highest_lines_cleared = -1, lowest_highest_line = -1;
	unsigned long lowest_hole_count = ( unsigned long )-1;

	//Set up a new board to use for temporary calculations.
	if( !new_board( &board, game->board.width, game->board.height ) )
		return NULL;
//TODO: OPTIMISE THE HOLE COUNTING ROUTINE, BOARD COPY, LINE CHECK ROUTINES
	//Iterate through each node in the list.
	for( ; end_list != NULL; end_list = end_list->next )
	{
		//Create a temporary copy of the game's board.
		copy_board( &board, &game->board );

		//Set the block represented by the current end point in the temporary board.
		set_shape( &end_list->block, &board, &game->shape_set );

		//Clear any lines in the board that are now complete, and store the amount cleared.
		lines_cleared = check_lines( &board );
//TODO: COMPLETELY SKIP AN END-POINT THAT RESULTS IN LOSS
//TODO: PREFER NOT TO PLACE A BLOCK OVER AN ALREADY-ENCLOSED HOLE
		//Firstly, check the amount of lines cleared.
/*		if( lines_cleared > highest_lines_cleared )
		{
			best_end_point = end_list;
			highest_lines_cleared = lines_cleared;
		}
		else if( lines_cleared == highest_lines_cleared )
		{
*/			//Then check the amount of holes in the board.
			hole_count = count_holes( &board );
			if( hole_count < lowest_hole_count )
			{
				best_end_point = end_list;
				lowest_hole_count = hole_count;
			}
			else if( hole_count == lowest_hole_count )
			{
				//Then check the highest line in the board.
				if( board.highest_line > lowest_highest_line )
				{
					best_end_point = end_list;
					lowest_highest_line = board.highest_line;
				}
				else if( board.highest_line == lowest_highest_line )
				{
					//Finally, check the lowest block.
					if( end_list->block.y > lowest_y )
					{
						best_end_point = end_list;
						lowest_y = end_list->block.y;
					}
				}
			}
//		}
	}

	return best_end_point;
}

//Creates a new linked list using the given end point and its parents.
static ai_node_struct *ai_process_pathfind_construct_path_from_end_point( ai_node_struct *end_point )
{
	ai_node_struct *next = NULL;

	while( end_point != NULL )
	{
		end_point->next = next;
		next = end_point;
		end_point = end_point->parent;
		next->prev = end_point;
	}

	return next;
}

//Sets the path for the current block in the given game to follow.
static ai_node_struct *ai_process_pathfind_get_path( const game_struct *const game )
{
	ai_node_struct *current_node, *path, *open_list, *closed_list, *end_list;

	//Initialise the lists to an empty state.
	open_list = NULL;
	closed_list = NULL;
	end_list = NULL;

	//Add the current block position to the open list as the starting node.
	current_node = ai_process_pathfind_new_node( &open_list, NULL, &game->current_block, MOVE_OPERATION_NONE );
	if( current_node == NULL )
		return NULL;

	//Pathfind to populate the list of possible end points.
	ai_process_pathfind_find_end_points( &open_list, &closed_list, &end_list, game );
	if( end_list == NULL )
	{
		ai_process_pathfind_free_node_list( open_list );
		ai_process_pathfind_free_node_list( closed_list );
		return NULL;
	}

	//Free the open list if it still has any nodes in it.
	ai_process_pathfind_free_node_list( open_list );

	//Determine the end point to use.
	path = ai_process_pathfind_choose_best_end_point( end_list, game );

	//Remove the end point from the list of end points.
	ai_process_pathfind_remove_node( path, &end_list );

	//Free the list of end points now that we've extracted the node we want.
	ai_process_pathfind_free_node_list( end_list );

	//For all of the nodes that are part of the path to this end point,
	//remove them from the closed list.
	ai_process_pathfind_remove_all_nodes_to_end_point_from_list( path, &closed_list );

	//Free the closed list now that we've extracted the nodes we want.
	ai_process_pathfind_free_node_list( closed_list );

	//Construct a new list/path using the nodes leading up to and including the end point.
	path = ai_process_pathfind_construct_path_from_end_point( path );

	return path;
}

//Should be called whenever a new block is created in a game so that the AI can determine where to move it to.
void ai_new_block( const game_struct *const game )
{
	ai_process_pathfind_free_node_list( game->ai->path );
	game->ai->path = ai_process_pathfind_get_path( game );
	game->ai->current_node = game->ai->path;
}

//Searches the given linked list of nodes and determines if each of the
//nodes require the given move operation to be moved to.
static unsigned char ai_process_pathfind_all_nodes_require_move_operation( const ai_node_struct *list, move_operation_enum move_operation )
{
	//Iterate through all nodes starting from the supplied node.
	for( ; list != NULL; list = list->next )
	{
		//Check if the node requires a move operation other than the one supplied.
		if( list->move_operation != move_operation )
			return FALSE;
	}

	return TRUE;
}

//Has the AI player disable the given game's turbo function.
static void ai_process_disable_turbo( game_struct *const game )
{
	//Check if the turbo key is currently down.
	if( game->key[ GAME_KEY_TURBO ].state.down )
	{
		//If so, release it.
		game->key[ GAME_KEY_TURBO ].state.pressed = 0;
		game->key[ GAME_KEY_TURBO ].state.down = 0;
		game->key[ GAME_KEY_TURBO ].state.released = 1;
	}
}

//Has the AI player enable the given game's turbo function.
static void ai_process_enable_turbo( game_struct *const game )
{
	//Check if the turbo key is currently down.
	if( !game->key[ GAME_KEY_TURBO ].state.down )
	{
		//If not, press and hold it.
		game->key[ GAME_KEY_TURBO ].state.pressed = 1;
		game->key[ GAME_KEY_TURBO ].state.down = 1;
		game->key[ GAME_KEY_TURBO ].state.released = 0;
	}
}

//Should be called regularly to allow the AI player to determine what to do.
void ai_process( game_struct *const game )
{
	ai_node_struct *next_node;
//DEBUG
//ai_new_block( game );
	//Firstly check if the current block position is still
	//that represented by the current node.
	while( memcmp( &game->ai->current_node->block, &game->current_block, sizeof( block_struct ) ) != 0 )
	{
		//If it differs, set the current node to the next node, since
		//it's most likely successfully moved.
		game->ai->current_node = game->ai->current_node->next;

		//If none of the nodes in the path match the current position,
		//the block is not inside the path.
		if( game->ai->current_node == NULL )
		{
			//So what we'll do is act as if we have a new block so that a
			//new path will be created.
			ai_new_block( game );
			break;
		}
	}

	//Store a pointer to the next node to avoid excessive dereferencing.
	next_node = game->ai->current_node->next;

	//Check if the current node is the end of the path.
	if( next_node == NULL )
	{
		//If so, the block is at the destination, so use the instant
		//drop key to set the current block.
#ifdef AI_USE_INSTANT_DROP
		game->key[ GAME_KEY_DROP ].state.pressed = 1;
#elif defined AI_USE_TURBO
		ai_process_enable_turbo( game );
#endif
		return;
	}

	//Press the keys needed to get to the orientation represented by the next node.
	switch( next_node->move_operation )
	{
		case MOVE_OPERATION_LEFT:
			ai_process_disable_turbo( game );
			game->key[ GAME_KEY_LEFT ].state.pressed = 1;
			break;
		case MOVE_OPERATION_RIGHT:
			ai_process_disable_turbo( game );
			game->key[ GAME_KEY_RIGHT ].state.pressed = 1;
			break;
		case MOVE_OPERATION_DOWN:
			//If the block has to go directly down, first check if all of the nodes
			//to the destination require a down move.
#ifdef AI_USE_INSTANT_DROP
			if( ai_process_pathfind_all_nodes_require_move_operation( next_node, MOVE_OPERATION_DOWN ) )
			{
				//If so, use the instant drop key.
				game->key[ GAME_KEY_DROP ].state.pressed = 1;
			}
			else
			{
				//Otherwise enable turbo.
#ifdef AI_USE_TURBO
				ai_process_enable_turbo( game );
#endif
			}
#elif defined AI_USE_TURBO
			ai_process_enable_turbo( game );
#endif
			break;
		case MOVE_OPERATION_CLOCKWISE:
			ai_process_disable_turbo( game );
			game->key[ GAME_KEY_ROTATE_CLOCKWISE ].state.pressed = 1;
			break;
		case MOVE_OPERATION_COUNTERCLOCKWISE:
			ai_process_disable_turbo( game );
			game->key[ GAME_KEY_ROTATE_COUNTERCLOCKWISE ].state.pressed = 1;
			break;
		case MOVE_OPERATION_NONE:
		default:
			break;
	}
}
