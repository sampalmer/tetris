#ifndef _CONFIG_H_
#define _CONFIG_H_

typedef struct
{
	char *key;
	char *value;
} config_item_struct;

typedef struct
{
	config_item_struct *item;
	unsigned short item_count;
} config_section_struct;

typedef struct
{
	config_section_struct *section;
	unsigned short section_count;
} config_struct;

config_struct *config_load( const char *file );
void config_free( config_struct *config );

#endif
