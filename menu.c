#include <stdlib.h>
#include <string.h>
#include <stdarg.h>
#include "menu.h"

//Creates a menu. Returns a pointer to it.
menu_struct *menu_create( menu_struct *parent, menu_type_enum type, const char *title )
{
	menu_struct *menu;
	
	//Validate.
	if( title == NULL )
		return NULL;

	//Allocate memory for the menu.
	menu = ( menu_struct * )malloc( sizeof( menu_struct ) );
	if( menu == NULL )
		return NULL;
	
	//Allocate memory for and copy the title string.
	menu->title = _strdup( title );
	if( menu->title == NULL )
	{
		free( menu );
		return NULL;
	}
	
	//Fill in the menu properties.
	menu->type = type;
	menu->parent = parent;
	menu->item = NULL;
	menu->item_count = 0;
	
	//Default call-backs to NULL.
	menu->apply = NULL;
	menu->apply_userdata = NULL;
	menu->terminate = NULL;
	menu->terminate_userdata = NULL;

	return menu;
}

//Appends a menu item to a menu.
static menu_item_struct *menu_append_item( menu_struct *menu, menu_item_struct *item )
{
	menu_item_struct *_item;
	unsigned char new_item_count;
	
	//Validate.
	if( menu == NULL || item == NULL )
		return NULL;

	//Increase the size of the array of menu items.
	new_item_count = menu->item_count + 1;
	_item = menu->item;
	menu->item = ( menu_item_struct * )realloc( _item, sizeof( menu_item_struct ) * new_item_count );
	if( menu->item == NULL )
	{
		menu->item = _item;
		return NULL;
	}
	menu->item_count = new_item_count;

	//Copy the new menu item in.
	_item = menu->item + new_item_count - 1;
	_item->type = item->type;
	_item->title = _strdup( item->title );
	if( _item->title == NULL )
	{
		menu->item_count = menu->item_count - 1;
		_item = menu->item;
		menu->item = ( menu_item_struct * )realloc( _item, sizeof( menu_item_struct ) * menu->item_count );
		if( menu->item == NULL && menu->item_count != 0 )
			menu->item = _item;

		return NULL;
	}
	
	//Now copy the item data in.
	switch( item->type )
	{
		case MENU_ITEM_DESTROY:
			_item->data.return_val = item->data.return_val;
			break;
		case MENU_ITEM_SUBMENU:
			_item->data.submenu = item->data.submenu;
			break;
		case MENU_ITEM_ONOFF:
			_item->data.on = item->data.on;
			break;
		case MENU_ITEM_INTEGER:
			memcpy( &_item->data.integer, &item->data.integer, sizeof( item->data.integer ) );
			break;
		case MENU_ITEM_STRINGS:
		{
			char **_string, **string, **last_string;
			
			//Copy the string count and the index of the currently selected string.
			_item->data.strings.count = item->data.strings.count;
			_item->data.strings.val = item->data.strings.val;

			//Allocate memory for the array of pointers.
			_item->data.strings.string = ( char ** )malloc( sizeof( char * ) * _item->data.strings.count );
			if( _item->data.strings.string == NULL && _item->data.strings.count != 0 )
				goto appendfailed;
			
			_string = _item->data.strings.string;
			last_string = _string + _item->data.strings.count;
			string = item->data.strings.string;
			
			//Iterate through each string and copy it.
			for( ; _string < last_string; ++_string, ++string )
			{
				*_string = _strdup( *string );
				if( _string == NULL )
				{
					//If this fails, free the previously copied strings and the array and return.
					last_string = _string;
					_string = _item->data.strings.string;

					for( ; _string < last_string; ++_string )
						free( *_string );

					free( _item->data.strings.string );
					goto appendfailed;
				}
			}

			break;
		}
		case MENU_ITEM_STRING:
			_item->data.string.len = item->data.string.len;
			_item->data.string.max_len = item->data.string.max_len;
			_item->data.string.string = ( char * )malloc( sizeof( char ) * _item->data.string.len );
			if( _item->data.string.string == NULL )
				goto appendfailed;
			memcpy( _item->data.string.string, item->data.string.string, sizeof( char ) * _item->data.string.len );

			break;
		default:
			goto appendfailed;
	}
	
	return _item;

appendfailed:
	free( _item->title );

	menu->item_count = menu->item_count - 1;
	_item = menu->item;
	menu->item = ( menu_item_struct * )realloc( _item, sizeof( menu_item_struct ) * menu->item_count );
	if( menu->item == NULL && menu->item_count != 0 )
		menu->item = _item;

	return NULL;
}

menu_item_struct *menu_append_destroy_item( menu_struct *menu, unsigned char id, char *title, int return_val )
{
	menu_item_struct item;

	item.type = MENU_ITEM_DESTROY;
	item.id = id;
	item.title = title;
	item.data.return_val = return_val;

	return menu_append_item( menu, &item );
}

menu_item_struct *menu_append_submenu_item( menu_struct *menu, unsigned char id, char *title, menu_struct *submenu )
{
	menu_item_struct item;

	item.type = MENU_ITEM_SUBMENU;
	item.id = id;
	item.title = title;
	item.data.submenu = submenu;

	return menu_append_item( menu, &item );
}

menu_item_struct *menu_append_onoff_item( menu_struct *menu, unsigned char id, char *title, unsigned char on )
{
	menu_item_struct item;

	item.type = MENU_ITEM_ONOFF;
	item.id = id;
	item.title = title;
	item.data.on = on;

	return menu_append_item( menu, &item );
}

menu_item_struct *menu_append_integer_item( menu_struct *menu, unsigned char id, char *title, int lower_boundary, int upper_boundary, int val )
{
	menu_item_struct item;

	item.type = MENU_ITEM_INTEGER;
	item.id = id;
	item.title = title;
	item.data.integer.lower_boundary = lower_boundary;
	item.data.integer.upper_boundary = upper_boundary;
	item.data.integer.val = val;

	return menu_append_item( menu, &item );
}

menu_item_struct *menu_append_strings_item( menu_struct *menu, unsigned char id, char *title, unsigned char index, unsigned char string_count, ... )
{
	menu_item_struct item, *pitem;
	va_list va;
	unsigned char i;

	item.type = MENU_ITEM_STRINGS;
	item.id = id;
	item.title = title;
	
	//Allocate memory for the array of strings.
	item.data.strings.val = index;
	item.data.strings.count = string_count;
	item.data.strings.string = ( char ** )malloc( sizeof( char * ) * string_count );
	if( item.data.string.string == NULL )
		return NULL;
	
	//Copy in a pointer to each string.
	va_start( va, string_count );
	for( i = 0; i < string_count; ++i )
		item.data.strings.string[ i ] = va_arg( va, char * );
	va_end( va );

	pitem = menu_append_item( menu, &item );
	free( item.data.strings.string );

	return pitem;
}

menu_item_struct *menu_append_string_item( menu_struct *menu, unsigned char id, char *title, unsigned short len, unsigned short max_len, char *string )
{
	menu_item_struct item;

	item.type = MENU_ITEM_STRING;
	item.id = id;
	item.title = title;
	item.data.string.len = len;
	item.data.string.max_len = max_len;
	item.data.string.string = string;

	return menu_append_item( menu, &item );
}

//Frees a menu, including its menu items.
void menu_free( menu_struct *menu )
{
	menu_item_struct *item, *last_item;

	//Validate.
	if( menu == NULL )
		return;

	free( menu->title );
	
	item = menu->item;
	last_item = item + menu->item_count;

	//Iterate through each menu item.
	for( ; item < last_item; ++item )
	{
		free( item->title );

		switch( item->type )
		{
			case MENU_ITEM_STRINGS:
			{
				char **string, **last_string;

				string = item->data.strings.string;
				last_string = string + item->data.strings.count;

				for( ; string < last_string; ++string )
					free( *string );

				free( item->data.strings.string );

				break;
			}
			case MENU_ITEM_STRING:
				free( item->data.string.string );
				break;
			default:
				break;
		}
	}

	free( menu->item );
}
