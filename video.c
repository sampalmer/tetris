#include <stdlib.h>
#include "main.h"
#include "video.h"

#include "video_sdl.h"
#include "video_opengl.h"

//Function pointers.
static unsigned char ( *_video_init )( const video_info_struct *const video, const unsigned char texture_count ) = NULL;
static void ( *_video_deinit )() = NULL;
static void ( *_video_toggle_fullscreen )() = NULL;
static unsigned char ( *_video_load_texture )( const char *const file, const unsigned long id ) = NULL;
static unsigned char ( *_video_reload_texture )( const unsigned long index ) = NULL;
static void ( *_video_free_texture )( const unsigned long index ) = NULL;
static unsigned char ( *_video_reload_all_textures )() = NULL;
static void ( *_video_free_all_textures )() = NULL;
static void ( *_video_blit_texture )( const unsigned long index, const long x, const long y ) = NULL;
static void ( *_video_refresh )() = NULL;
static void ( *_video_set_clear_colour )( unsigned char r, unsigned char g, unsigned char b ) = NULL;
static void ( *_video_clear )() = NULL;

static void video_set_sdl_functions()
{
}

static void video_set_opengl_functions()
{
	_video_init = video_init_opengl;
	_video_deinit = video_deinit_opengl;
	_video_toggle_fullscreen = video_toggle_fullscreen_opengl;
	_video_load_texture = video_load_texture_opengl;
	_video_reload_texture = video_reload_texture_opengl;
	_video_free_texture = video_free_texture_opengl;
	_video_reload_all_textures = video_reload_all_textures_opengl;
	_video_free_all_textures = video_free_all_textures_opengl;
	_video_blit_texture = video_blit_texture_opengl;
	_video_refresh = video_refresh_opengl;
	_video_set_clear_colour = video_set_clear_colour_opengl;
	_video_clear = video_clear;
}

static void video_unset_functions()
{
	_video_init = NULL;
	_video_deinit = NULL;
	_video_toggle_fullscreen = NULL;
	_video_load_texture = NULL;
	_video_reload_texture = NULL;
	_video_free_texture = NULL;
	_video_reload_all_textures = NULL;
	_video_free_all_textures = NULL;
	_video_blit_texture = NULL;
	_video_refresh = NULL;
	_video_set_clear_colour = NULL;
	_video_clear = NULL;
}

unsigned char video_init( const video_info_struct *const video_info, const unsigned char texture_count )
{
	unsigned char ret_val;
	
	//Set the video function pointers depending on the renderer.
	switch( video->renderer )
	{
		case VIDEO_SDL:
			video_set_sdl_functions();
			break;
		case VIDEO_OPENGL:
			video_set_opengl_functions();
			break;
		default:
			return FALSE;
	}
	
	//Initialise the video.
	if( !_video_init( video, texture_count ) )
	{
		video_unset_functions();
		return FALSE;
	}

	return TRUE;
}

void video_deinit()
{
	_video_deinit();
	video_unset_functions();
}

__inline__ void video_toggle_fullscreen()
{
	_video_toggle_fullscreen();
}

__inline__ unsigned char video_load_texture( const char *const file, const unsigned long id )
{
	return _video_load_texture( file, index );
}

__inline__ unsigned char video_reload_texture( const unsigned long index )
{
	return _video_reload_texture( index );
}

__inline__ void video_free_texture( const unsigned long index )
{
	_video_free_texture( index );
}

__inline__ unsigned char video_reload_all_textures()
{
	return _video_reload_all_textures();
}

__inline__ void video_free_all_textures()
{
	_video_free_all_textures();
}

__inline__ void video_blit_texture( const unsigned long index, const long x, const long y )
{
	_video_blit_texture( index, x, y );
}

__inline__ void video_refresh()
{
	_video_refresh();
}

__inline__ void video_set_clear_colour( unsigned char r, unsigned char g, unsigned char b )
{
	_video_set_clear_colour( r, g, b );
}

__inline__ void video_clear()
{
	_video_clear();
}
